package noriko.date;
import noriko.date.Qreki.Rokuyou;

enum Rokuyou {
	taian;
	shakku;
	senshou;
	tomobiki;
	senbu;
	butsumetsu;
}

/**
 * 旧暦を扱う Qreki
 * 
 * 以下を参考に作成しました
 * http://www.vector.co.jp/soft/dos/personal/se016093.html
 * 旧暦計算サンプルプログラム  $Revision:   1.1  $
 * Coded by H.Takano 1993,1994
 * 
 * @author あるる（きのもと 結衣）
 */
class Qreki
{
	static public var timeZone = 9.0 / 24.0;

	/**
	 * 年月日から旧暦を計算する
	 * @param	y	西暦
	 * @param	m	月 1-12
	 * @param	d	日 1-31
	 * @return
	 */
	static public function fromYMD( y:Int, m:Int, d:Int ):Qreki
	{
		return Qreki.fromDate( new Date( y, m - 1, d, 0, 0, 0 ) );
	}

	/**
	 * Date型から旧暦に変換する
	 * @param	d
	 * @return
	 */
	static public function fromDate( d:Date ):Qreki
	{
		var jd = DateUtil.getJulianDate( d );
		return Qreki.fromJulianDate( jd );
	}

	/**
	 * ユリウス日から旧暦に変換する
	 * @param	jd
	 * @return
	 */
	static public function fromJulianDate( jd:Float ):Qreki
	{
		var tm = Math.ffloor( jd );
		var chu = new Array<Array<Float>>( );
		chu.push( Qreki.beforeNibunFromJulianDate( tm ) );
		for ( i in 1 ... 4 ) {
			chu.push( Qreki.chukiFromJulianDate( chu[i-1][0] + 32 ) );
		}

		var saku = new Array<Float>( );
		saku.push( Qreki.sakuFromJulianDate( chu[0][0] ) );
		for( i in 1 ... 5 ) {
			saku.push( Qreki.sakuFromJulianDate( saku[i - 1] + 30 ) );
			if( Math.abs( Math.ffloor( saku[i-1] ) - Math.ffloor( saku[i] ) ) <= 26 ) {
				saku[i] = Qreki.sakuFromJulianDate( saku[i-1] + 35 );
			}
		}
		if( Math.ffloor( saku[1] ) <= Math.ffloor( chu[0][0] ) ) {
			for ( i in 0 ... 4 ) saku[i] = saku[i + 1];
			saku[4] = Qreki.sakuFromJulianDate( saku[3] + 35 );
		}else if( Math.ffloor( chu[0][0] ) < Math.ffloor( saku[0] ) ){
			for ( i in [ 4, 3, 2, 1 ] ) saku[i] = saku[i - 1];
			saku[0] = Qreki.sakuFromJulianDate( saku[0] - 27 );
		}

		var leap = ( Math.ffloor( saku[4] ) <= Math.ffloor( chu[3][0] ) );
		var m = new Array<{month:Int, leapMonth:Bool, jd:Float}>( );
		for ( i in 0 ... 5 ) {
			m.push({month:0, leapMonth:false, jd:0});
		}
		m[0].month = Math.floor( chu[0][1] / 30 ) + 2;
		m[0].leapMonth = false;
		m[0].jd = Math.ffloor( saku[0] );
		for ( i in 1 ... 5 ) {
			if( leap && ( i != 1 ) ) {
				if (
					( Math.ffloor( chu[i - 1][0] ) <= Math.ffloor( saku[i - 1] ) ) &&
					( Math.ffloor( saku[i] ) <= Math.ffloor( chu[i - 1][0] ) )
				) {
					m[i - 1].month = m[i - 2].month;
					m[i - 1].leapMonth = true;
					m[i - 1].jd = Math.ffloor( saku[i - 1] );
					leap = false;
				}
			}
			m[i].month = m[i - 1].month + 1;
			if ( 12 < m[i].month ) m[i].month -= 12;
			m[i].leapMonth = false;
			m[i].jd = Math.ffloor( saku[i] );
		}

		var state = 0;
		var cnt = 0;
		for ( i in 0 ... 5 ) {
			if ( tm < m[i].jd ) {
				state = 1;
				break;
			}else if( tm == m[i].jd ) {
				state = 2;
				break;
			}
			cnt ++;
		}
		if ( ( state == 0 )||( state == 1 ) ) cnt --;

		var qreki = new Qreki( );

		qreki.newDate = DateUtil.makeJulianDate( jd );
		var newMonth = qreki.newDate.getMonth( ) + 1;
		qreki.year = qreki.newDate.getFullYear( );
		qreki.month = m[cnt].month;
		qreki.date = Math.floor( tm - m[cnt].jd + 1 );
		qreki.leapMonth = m[cnt].leapMonth;
		if( ( 9 < qreki.month )&&( newMonth < qreki.month ) ) qreki.year --;

		return qreki;
	}

	/**
	 * 中気の計算
	 * @param	tm
	 * @return
	 */
	static private function chukiFromJulianDate( tm:Float ):Array<Float>
	{
		var tm1 = Math.ffloor( tm );
		var tm2 = ( tm - tm1 ) - Qreki.timeZone;
		var t = ( tm2 + 0.5 ) / 36525 + ( tm1 - 2451545 ) / 36525;
		var rm_sun = Qreki.longitudeOfSun( t );
		var rm_sun0 = rm_sun - rm_sun % 30.0;
		var delta_t1 = 0.0;
		var delta_t2 = 1.0;
		while( 1.0 / 86400 < Math.abs( delta_t1 + delta_t2 ) ) {
			t = ( tm2 + 0.5 ) / 36525 + ( tm1 - 2451545 ) / 36525;
			rm_sun = Qreki.longitudeOfSun( t );
			var delta_rm = rm_sun - rm_sun0;
			if( 180 < delta_rm ) {
				delta_rm -= 360;
			}else if( delta_rm < -180 ){
				delta_rm += 360;
			}
			var d = delta_rm * 365.2 / 360;
			delta_t1 = Math.ffloor( d );
			delta_t2 = d - delta_t1;
			tm1 -= delta_t1;
			tm2 -= delta_t2;
			if( tm2 < 0 ) {
				tm2 += 1.0;
				tm1 -= 1.0;
			}
		}

		return [ tm1 + tm2 + Qreki.timeZone, rm_sun0 ];
	}

	/**
	 * 直前の二分二至の時刻を計算
	 * @param	tm
	 * @return
	 */
	static public function beforeNibunFromJulianDate( tm:Float ):Array<Float>
	{
		var tm1 = Math.ffloor( tm );
		var tm2 = ( tm - tm1 ) - Qreki.timeZone;
		var t = ( tm2 + 0.5 ) / 36525 + ( tm1 - 2451545 ) / 36525;
		var rm_sun = Qreki.longitudeOfSun( t );
		var rm_sun0 = rm_sun - rm_sun % 90.0;
		var delta_t1 = 0.0;
		var delta_t2 = 1.0;
		while( 1.0 / 86400 < Math.abs( delta_t1 + delta_t2 ) ) {
			t = ( tm2 + 0.5 ) / 36525 + ( tm1 - 2451545 ) / 36525;
			rm_sun = Qreki.longitudeOfSun( t );
			var delta_rm = rm_sun - rm_sun0;
			if( 180 < delta_rm ) {
				delta_rm -= 360;
			}else if( delta_rm < -180 ){
				delta_rm += 360;
			}
			var d = delta_rm * 365.2 / 360;
			delta_t1 = Math.ffloor( d );
			delta_t2 = d - delta_t1;
			tm1 -= delta_t1;
			tm2 -= delta_t2;
			if( tm2 < 0 ) {
				tm2 += 1.0;
				tm1 -= 1.0;
			}
		}

		return [ tm1 + tm2 + Qreki.timeZone, rm_sun0 ];
	}

	/**
	 * 直近の朔の時刻を計算
	 * @param	tm
	 * @return
	 */
	static private function sakuFromJulianDate( tm:Float ):Float
	{
		var tm1 = Math.ffloor( tm );
		var tm2 = ( tm - tm1 ) - Qreki.timeZone;
		var delta_t1 = 0.0;
		var delta_t2 = 1.0;
		for( lc in 1 ... 30 ) {
			var t = ( tm2 + 0.5 ) / 36525 + ( tm1 - 2451545 ) / 36525;
			var rm_sun = Qreki.longitudeOfSun( t );
			var rm_moon = Qreki.longitudeOfMoon( t );
			var delta_rm = rm_moon - rm_sun;
			if( ( lc == 1 )&&( delta_rm < 0.0 ) ) {
				delta_rm = delta_rm % 360.0;
			}else if( ( 0.0 <= rm_sun )&&( rm_sun <= 20 )&&( 300 <= rm_moon ) ) {
				delta_rm = 360 - ( delta_rm % 360 );
			}else if( 40 < Math.abs( delta_rm ) ) {
				delta_rm = delta_rm % 360.0;
			}
			var d = delta_rm * 29.530589 / 360;
			delta_t1 = Math.ffloor( d );
			delta_t2 = d - delta_t1;
			tm1 -= delta_t1;
			tm2 -= delta_t2;
			if( tm2 < 0 ) {
				tm2 += 1.0;
				tm1 -= 1.0;
			}
			if ( 1 / 86400 < ( Math.abs( delta_t1 + delta_t2 ) ) ) {
				if( lc == 15 ) {
					tm1 = tm - 26;
					tm2 = 0.0;
				}
			}else {
				break;
			}
		}

		return tm1 + tm2 + Qreki.timeZone;
	}

	/**
	 * 太陽の黄経
	 * @param	t
	 * @return
	 */
	static private function longitudeOfSun( t:Float ):Float
	{
		var k = Math.PI / 180;

		var ang = (31557.0 * t + 161.0) % 360.0;
		var th = .0004 * Math.cos(k * ang);
		ang = (29930.0 * t + 48.0) % 360.0;
		th += .0004 * Math.cos(k * ang);
		ang = (2281.0 * t + 221.0) % 360.0;
		th += .0005 * Math.cos(k * ang);
		ang = (155.0 * t + 118.0) % 360.0;
		th += .0005 * Math.cos(k * ang);
		ang = (33718.0 * t + 316.0) % 360.0;
		th += .0006 * Math.cos(k * ang);
		ang = (9038.0 * t + 64.0) % 360.0;
		th += .0007 * Math.cos(k * ang);
		ang = (3035.0 * t + 110.0) % 360.0;
		th += .0007 * Math.cos(k * ang);
		ang = (65929.0 * t + 45.0) % 360.0;
		th += .0007 * Math.cos(k * ang);
		ang = (22519.0 * t + 352.0) % 360.0;
		th += .0013 * Math.cos(k * ang);
		ang = (45038.0 * t + 254.0) % 360.0;
		th += .0015 * Math.cos(k * ang);
		ang = (445267.0 * t + 208.0) % 360.0;
		th += .0018 * Math.cos(k * ang);
		ang = (19.0 * t + 159.0) % 360.0;
		th += .0018 * Math.cos(k * ang);
		ang = (32964.0 * t + 158.0) % 360.0;
		th += .0020 * Math.cos(k * ang);
		ang = (71998.1 * t + 265.1) % 360.0;
		th += .0200 * Math.cos(k * ang);

		ang = (35999.05 * t + 267.52) % 360.0;
		th -= 0.0048 * t * Math.cos(k * ang);
		th += 1.9147 * Math.cos(k * ang);

		ang = (36000.7695 * t) % 360.0;
		ang = (ang + 280.4659) % 360.0;
		th = (th + ang) % 360.0;

		return th;
	}

	/**
	 * 月の黄経
	 * @param	t
	 * @return
	 */
	static private function longitudeOfMoon( t:Float ):Float
	{
		var k = Math.PI / 180;
		var ang = (2322131.0 * t + 191.0) % 360.0;
		var th = .0003 * Math.cos(k * ang);
		ang = (4067.0 * t + 70.0) % 360.0;
		th += .0003 * Math.cos(k * ang);
		ang = (549197.0 * t + 220.0) % 360.0;
		th += .0003 * Math.cos(k * ang);
		ang = (1808933.0 * t + 58.0) % 360.0;
		th += .0003 * Math.cos(k * ang);
		ang = (349472.0 * t + 337.0) % 360.0;
		th += .0003 * Math.cos(k * ang);
		ang = (381404.0 * t + 354.0) % 360.0;
		th += .0003 * Math.cos(k * ang);
		ang = (958465.0 * t + 340.0) % 360.0;
		th += .0003 * Math.cos(k * ang);
		ang = (12006.0 * t + 187.0) % 360.0;
		th += .0004 * Math.cos(k * ang);
		ang = (39871.0 * t + 223.0) % 360.0;
		th += .0004 * Math.cos(k * ang);
		ang = (509131.0 * t + 242.0) % 360.0;
		th += .0005 * Math.cos(k * ang);
		ang = (1745069.0 * t + 24.0) % 360.0;
		th += .0005 * Math.cos(k * ang);
		ang = (1908795.0 * t + 90.0) % 360.0;
		th += .0005 * Math.cos(k * ang);
		ang = (2258267.0 * t + 156.0) % 360.0;
		th += .0006 * Math.cos(k * ang);
		ang = (111869.0 * t + 38.0) % 360.0;
		th += .0006 * Math.cos(k * ang);
		ang = (27864.0 * t + 127.0) % 360.0;
		th += .0007 * Math.cos(k * ang);
		ang = (485333.0 * t + 186.0) % 360.0;
		th += .0007 * Math.cos(k * ang);
		ang = (405201.0 * t + 50.0) % 360.0;
		th += .0007 * Math.cos(k * ang);
		ang = (790672.0 * t + 114.0) % 360.0;
		th += .0007 * Math.cos(k * ang);
		ang = (1403732.0 * t + 98.0) % 360.0;
		th += .0008 * Math.cos(k * ang);
		ang = (858602.0 * t + 129.0) % 360.0;
		th += .0009 * Math.cos(k * ang);
		ang = (1920802.0 * t + 186.0) % 360.0;
		th += .0011 * Math.cos(k * ang);
		ang = (1267871.0 * t + 249.0) % 360.0;
		th += .0012 * Math.cos(k * ang);
		ang = (1856938.0 * t + 152.0) % 360.0;
		th += .0016 * Math.cos(k * ang);
		ang = (401329.0 * t + 274.0) % 360.0;
		th += .0018 * Math.cos(k * ang);
		ang = (341337.0 * t + 16.0) % 360.0;
		th += .0021 * Math.cos(k * ang);
		ang = (71998.0 * t + 85.0) % 360.0;
		th += .0021 * Math.cos(k * ang);
		ang = (990397.0 * t + 357.0) % 360.0;
		th += .0021 * Math.cos(k * ang);
		ang = (818536.0 * t + 151.0) % 360.0;
		th += .0022 * Math.cos(k * ang);
		ang = (922466.0 * t + 163.0) % 360.0;
		th += .0023 * Math.cos(k * ang);
		ang = (99863.0 * t + 122.0) % 360.0;
		th += .0024 * Math.cos(k * ang);
		ang = (1379739.0 * t + 17.0) % 360.0;
		th += .0026 * Math.cos(k * ang);
		ang = (918399.0 * t + 182.0) % 360.0;
		th += .0027 * Math.cos(k * ang);
		ang = (1934.0 * t + 145.0) % 360.0;
		th += .0028 * Math.cos(k * ang);
		ang = (541062.0 * t + 259.0) % 360.0;
		th += .0037 * Math.cos(k * ang);
		ang = (1781068.0 * t + 21.0) % 360.0;
		th += .0038 * Math.cos(k * ang);
		ang = (133.0 * t + 29.0) % 360.0;
		th += .0040 * Math.cos(k * ang);
		ang = (1844932.0 * t + 56.0) % 360.0;
		th += .0040 * Math.cos(k * ang);
		ang = (1331734.0 * t + 283.0) % 360.0;
		th += .0040 * Math.cos(k * ang);
		ang = (481266.0 * t + 205.0) % 360.0;
		th += .0050 * Math.cos(k * ang);
		ang = (31932.0 * t + 107.0) % 360.0;
		th += .0052 * Math.cos(k * ang);
		ang = (926533.0 * t + 323.0) % 360.0;
		th += .0068 * Math.cos(k * ang);
		ang = (449334.0 * t + 188.0) % 360.0;
		th += .0079 * Math.cos(k * ang);
		ang = (826671.0 * t + 111.0) % 360.0;
		th += .0085 * Math.cos(k * ang);
		ang = (1431597.0 * t + 315.0) % 360.0;
		th += .0100 * Math.cos(k * ang);
		ang = (1303870.0 * t + 246.0) % 360.0;
		th += .0107 * Math.cos(k * ang);
		ang = (489205.0 * t + 142.0) % 360.0;
		th += .0110 * Math.cos(k * ang);
		ang = (1443603.0 * t + 52.0) % 360.0;
		th += .0125 * Math.cos(k * ang);
		ang = (75870.0 * t + 41.0) % 360.0;
		th += .0154 * Math.cos(k * ang);
		ang = (513197.9 * t + 222.5) % 360.0;
		th += .0304 * Math.cos(k * ang);
		ang = (445267.1 * t + 27.9) % 360.0;
		th += .0347 * Math.cos(k * ang);
		ang = (441199.8 * t + 47.4) % 360.0;
		th += .0409 * Math.cos(k * ang);
		ang = (854535.2 * t + 148.2) % 360.0;
		th += .0458 * Math.cos(k * ang);
		ang = (1367733.1 * t + 280.7) % 360.0;
		th += .0533 * Math.cos(k * ang);
		ang = (377336.3 * t + 13.2) % 360.0;
		th += .0571 * Math.cos(k * ang);
		ang = (63863.5 * t + 124.2) % 360.0;
		th += .0588 * Math.cos(k * ang);
		ang = (966404.0 * t + 276.5) % 360.0;
		th += .1144 * Math.cos(k * ang);
		ang = (35999.05 * t + 87.53) % 360.0;
		th += .1851 * Math.cos(k * ang);
		ang = (954397.74 * t + 179.93) % 360.0;
		th += .2136 * Math.cos(k * ang);
		ang = (890534.22 * t + 145.7) % 360.0;
		th += .6583 * Math.cos(k * ang);
		ang = (413335.35 * t + 10.74) % 360.0;
		th += 1.2740 * Math.cos(k * ang);
		ang = (477198.868 * t + 44.963) % 360.0;
		th += 6.2888 * Math.cos(k * ang);

		ang = (481267.8809 * t) % 360.0;
		ang = (ang + 218.3162) % 360.0;
		th = (th + ang) % 360.0;

		return th;
	}

	public var year:Int;
	public var month:Int;
	public var date:Int;
	public var leapMonth:Bool;
	public var newDate:Date;
	public var rokuyou(get, never):Rokuyou;

	public function new( ):Void {}

	public function get_rokuyou( ):Rokuyou
	{
		return [taian, shakku, senshou, tomobiki, senbu, butsumetsu][(this.month + this.date) % 6];
	}

	/**
	 * 六曜文字列を取得
	 * @param	r
	 * @return
	 */
	public function getRokuyouString( ):String
	{
		return switch( this.rokuyou ) {
			case taian: "大安";
			case shakku: "赤口";
			case senshou: "先勝";
			case tomobiki: "友引";
			case senbu: "先負";
			case butsumetsu: "仏滅";
		};
	}


}
