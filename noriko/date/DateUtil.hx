package noriko.date;

import haxe.Json;
import haxe.macro.Context;
import haxe.EnumFlags;

import noriko.str.CSV;

/**
 * 日付フラグ
 */
enum DateInfoFlag {
	holiday;			// 祝日フラグ

	substituteHoliday;	// 振替休日
	changeToHoliday;	// 休日に挟まれた平日の休日化
	leapYear;			// 閏年の日
}

/**
 * 日付情報
 */
typedef DateInfo = {
	d:Date,							// 日付
	name:String,					// 名前
	otherNames:Array<String>,		// 他の名前
	flags:EnumFlags<DateInfoFlag>,	// フラグ
};

typedef DateFixedColumn = {start:Null<Int>, expired:Null<Int>, month:Int, date:Int, name:String};
typedef WeekFixedColumn = {start:Null<Int>, expired:Null<Int>, month:Int, week:Int, day:Int, name:String};

/**
 * 日付まわりのユーティリティ
 * @author あるる（きのもと 結衣）
 */
class DateUtil
{
	/**
	 * 十干を返す
	 * @param	date
	 * @return
	 */
	static public function getZodiac10( date:Date ):String
	{
		var table = [
			"甲", "乙", "丙", "丁", "戊", "己", "庚", "辛", "壬", "癸"
		];

		return table[( date.getFullYear() + 6 ) % 10];
	}

	/**
	 * 十二支を返す
	 * @param	date
	 * @return
	 */
	static public function getZodiac12( date:Date ):String
	{
		var table = [
			"子", "丑", "寅", "卯", "辰", "巳", "午", "未", "申", "酉", "戌", "亥"
		];

		return table[( date.getFullYear() + 8 ) % 12];
	}

	/**
	 * 誕生石を返す
	 * @param	date
	 * @return
	 */
	static public function getBirthstone( date:Date ):String
	{
		var table = [
			"ガーネット", "アメシスト", "アクアマリン", "ダイアモンド", "エメラルド",
			"パール", "ルビー", "サードニックス", "サファイア", "オパール", "トパーズ", "トルコ石"
		];

		return table[ date.getMonth( ) ];
	}

	/**
	 * 和暦を取得
	 * 
	 * Wikipediaの元号のページを参考に作成
	 * 
	 * @param	date
	 * @return
	 * 
	 * @see https://ja.wikipedia.org/wiki/%E5%85%83%E5%8F%B7%E4%B8%80%E8%A6%A7_(%E6%97%A5%E6%9C%AC)
	 */
	static public function getJapaneseDate( d:Date ):String
	{
		var table = [{"name":"令和","year":2019,"month":5,"date":1},{"name":"平成","year":1989,"month":1,"date":8},{"name":"昭和","year":1926,"month":12,"date":25},{"name":"大正","year":1912,"month":7,"date":30},{"name":"明治","year":1868,"month":1,"date":25},{"name":"慶応","year":1865,"month":5,"date":1},{"name":"元治","year":1864,"month":3,"date":27},{"name":"文久","year":1861,"month":3,"date":29},{"name":"万延","year":1860,"month":4,"date":8},{"name":"安政","year":1855,"month":1,"date":15},{"name":"嘉永","year":1848,"month":4,"date":1},{"name":"弘化","year":1845,"month":1,"date":9},{"name":"天保","year":1831,"month":1,"date":23},{"name":"文政","year":1818,"month":5,"date":26},{"name":"文化","year":1804,"month":3,"date":22},{"name":"享和","year":1801,"month":3,"date":19},{"name":"寛政","year":1789,"month":2,"date":19},{"name":"天明","year":1781,"month":4,"date":25},{"name":"安永","year":1772,"month":12,"date":10},{"name":"明和","year":1764,"month":6,"date":30},{"name":"宝暦","year":1751,"month":12,"date":14},{"name":"寛延","year":1748,"month":8,"date":5},{"name":"延享","year":1744,"month":4,"date":3},{"name":"寛保","year":1741,"month":4,"date":12},{"name":"元文","year":1736,"month":6,"date":7},{"name":"享保","year":1716,"month":8,"date":9},{"name":"正徳","year":1711,"month":6,"date":11},{"name":"宝永","year":1704,"month":4,"date":16},{"name":"元禄","year":1688,"month":10,"date":23},{"name":"貞享","year":1684,"month":4,"date":5},{"name":"天和","year":1681,"month":11,"date":9},{"name":"延宝","year":1673,"month":10,"date":30},{"name":"寛文","year":1661,"month":5,"date":23},{"name":"万治","year":1658,"month":8,"date":21},{"name":"明暦","year":1655,"month":5,"date":18},{"name":"承応","year":1652,"month":10,"date":20},{"name":"慶安","year":1648,"month":4,"date":7},{"name":"正保","year":1645,"month":1,"date":13},{"name":"寛永","year":1624,"month":4,"date":17},{"name":"元和","year":1615,"month":9,"date":5},{"name":"慶長","year":1596,"month":12,"date":16},{"name":"文禄","year":1593,"month":1,"date":10},{"name":"天正","year":1573,"month":8,"date":25},{"name":"元亀","year":1570,"month":5,"date":27},{"name":"永禄","year":1558,"month":3,"date":18},{"name":"弘治","year":1555,"month":11,"date":7},{"name":"天文","year":1532,"month":8,"date":29},{"name":"享禄","year":1528,"month":9,"date":3},{"name":"大永","year":1521,"month":9,"date":23},{"name":"永正","year":1504,"month":3,"date":16},{"name":"文亀","year":1501,"month":3,"date":18},{"name":"明応","year":1492,"month":8,"date":12},{"name":"延徳","year":1489,"month":9,"date":16},{"name":"長享","year":1487,"month":8,"date":9},{"name":"文明","year":1469,"month":6,"date":8},{"name":"応仁","year":1467,"month":4,"date":9},{"name":"文正","year":1466,"month":3,"date":14},{"name":"寛正","year":1461,"month":2,"date":1},{"name":"長禄","year":1457,"month":10,"date":16},{"name":"康正","year":1455,"month":9,"date":6},{"name":"享徳","year":1452,"month":8,"date":10},{"name":"宝徳","year":1449,"month":8,"date":16},{"name":"文安","year":1444,"month":2,"date":23},{"name":"嘉吉","year":1441,"month":3,"date":10},{"name":"永享","year":1429,"month":10,"date":3},{"name":"正長","year":1428,"month":6,"date":10},{"name":"応永","year":1394,"month":8,"date":2},{"name":"明徳","year":1390,"month":4,"date":12},{"name":"康応","year":1389,"month":3,"date":7},{"name":"嘉慶","year":1387,"month":10,"date":5},{"name":"至徳","year":1384,"month":3,"date":19},{"name":"永徳","year":1381,"month":3,"date":20},{"name":"康暦","year":1379,"month":4,"date":9},{"name":"永和","year":1375,"month":3,"date":29},{"name":"応安","year":1368,"month":3,"date":7},{"name":"貞治","year":1362,"month":10,"date":11},{"name":"康安","year":1361,"month":5,"date":4},{"name":"延文","year":1356,"month":4,"date":29},{"name":"文和","year":1352,"month":11,"date":4},{"name":"観応","year":1350,"month":4,"date":4},{"name":"貞和","year":1345,"month":11,"date":15},{"name":"康永","year":1342,"month":6,"date":1},{"name":"暦応","year":1338,"month":10,"date":11},{"name":"元中","year":1384,"month":5,"date":18},{"name":"弘和","year":1381,"month":3,"date":6},{"name":"天授","year":1375,"month":6,"date":26},{"name":"建徳","year":1370,"month":8,"date":16},{"name":"正平","year":1347,"month":1,"date":20},{"name":"興国","year":1340,"month":5,"date":25},{"name":"延元","year":1336,"month":4,"date":11},{"name":"建武","year":1334,"month":3,"date":5},{"name":"正慶","year":1332,"month":5,"date":23},{"name":"元弘","year":1331,"month":9,"date":11},{"name":"元徳","year":1329,"month":9,"date":22},{"name":"嘉暦","year":1326,"month":5,"date":28},{"name":"正中","year":1324,"month":12,"date":25},{"name":"元亨","year":1321,"month":3,"date":22},{"name":"元応","year":1319,"month":5,"date":18},{"name":"文保","year":1317,"month":3,"date":16},{"name":"正和","year":1312,"month":4,"date":27},{"name":"応長","year":1311,"month":5,"date":17},{"name":"延慶","year":1308,"month":11,"date":22},{"name":"徳治","year":1307,"month":1,"date":18},{"name":"嘉元","year":1303,"month":9,"date":16},{"name":"乾元","year":1302,"month":12,"date":10},{"name":"正安","year":1299,"month":5,"date":25},{"name":"永仁","year":1293,"month":9,"date":6},{"name":"正応","year":1288,"month":5,"date":29},{"name":"弘安","year":1278,"month":3,"date":23},{"name":"建治","year":1275,"month":5,"date":22},{"name":"文永","year":1264,"month":3,"date":27},{"name":"弘長","year":1261,"month":3,"date":22},{"name":"文応","year":1260,"month":5,"date":24},{"name":"正元","year":1259,"month":4,"date":20},{"name":"正嘉","year":1257,"month":3,"date":31},{"name":"康元","year":1256,"month":10,"date":24},{"name":"建長","year":1249,"month":5,"date":2},{"name":"宝治","year":1247,"month":4,"date":5},{"name":"寛元","year":1243,"month":3,"date":18},{"name":"仁治","year":1240,"month":8,"date":5},{"name":"延応","year":1239,"month":3,"date":13},{"name":"暦仁","year":1238,"month":12,"date":30},{"name":"嘉禎","year":1235,"month":11,"date":1},{"name":"文暦","year":1234,"month":11,"date":27},{"name":"天福","year":1233,"month":5,"date":25},{"name":"貞永","year":1232,"month":4,"date":23},{"name":"寛喜","year":1229,"month":3,"date":31},{"name":"安貞","year":1228,"month":1,"date":18},{"name":"嘉禄","year":1225,"month":5,"date":28},{"name":"元仁","year":1224,"month":12,"date":31},{"name":"貞応","year":1222,"month":5,"date":25},{"name":"承久","year":1219,"month":5,"date":27},{"name":"建保","year":1214,"month":1,"date":18},{"name":"建暦","year":1211,"month":4,"date":23},{"name":"承元","year":1207,"month":11,"date":16},{"name":"建永","year":1206,"month":6,"date":5},{"name":"元久","year":1204,"month":3,"date":23},{"name":"建仁","year":1201,"month":3,"date":19},{"name":"正治","year":1199,"month":5,"date":23},{"name":"建久","year":1190,"month":5,"date":16},{"name":"文治","year":1185,"month":9,"date":9},{"name":"元暦","year":1184,"month":5,"date":27},{"name":"寿永","year":1182,"month":6,"date":29},{"name":"養和","year":1181,"month":8,"date":25},{"name":"治承","year":1177,"month":8,"date":29},{"name":"安元","year":1175,"month":8,"date":16},{"name":"承安","year":1171,"month":5,"date":27},{"name":"嘉応","year":1169,"month":5,"date":6},{"name":"仁安","year":1166,"month":9,"date":23},{"name":"永万","year":1165,"month":7,"date":14},{"name":"長寛","year":1163,"month":5,"date":4},{"name":"応保","year":1161,"month":9,"date":24},{"name":"永暦","year":1160,"month":2,"date":18},{"name":"平治","year":1159,"month":5,"date":9},{"name":"保元","year":1156,"month":5,"date":18},{"name":"久寿","year":1154,"month":12,"date":4},{"name":"仁平","year":1151,"month":2,"date":14},{"name":"久安","year":1145,"month":8,"date":12},{"name":"天養","year":1144,"month":3,"date":28},{"name":"康治","year":1142,"month":5,"date":25},{"name":"永治","year":1141,"month":8,"date":13},{"name":"保延","year":1135,"month":6,"date":10},{"name":"長承","year":1132,"month":9,"date":21},{"name":"天承","year":1131,"month":2,"date":28},{"name":"大治","year":1126,"month":2,"date":15},{"name":"天治","year":1124,"month":5,"date":18},{"name":"保安","year":1120,"month":5,"date":9},{"name":"元永","year":1118,"month":4,"date":25},{"name":"永久","year":1113,"month":8,"date":25},{"name":"天永","year":1110,"month":7,"date":31},{"name":"天仁","year":1108,"month":9,"date":9},{"name":"嘉承","year":1106,"month":5,"date":13},{"name":"長治","year":1104,"month":3,"date":8},{"name":"康和","year":1099,"month":9,"date":15},{"name":"承徳","year":1097,"month":12,"date":27},{"name":"永長","year":1097,"month":1,"date":3},{"name":"嘉保","year":1095,"month":1,"date":23},{"name":"寛治","year":1087,"month":5,"date":11},{"name":"応徳","year":1084,"month":3,"date":15},{"name":"永保","year":1081,"month":3,"date":22},{"name":"承暦","year":1077,"month":12,"date":5},{"name":"承保","year":1074,"month":9,"date":16},{"name":"延久","year":1069,"month":5,"date":6},{"name":"治暦","year":1065,"month":9,"date":4},{"name":"康平","year":1058,"month":9,"date":19},{"name":"天喜","year":1053,"month":2,"date":2},{"name":"永承","year":1046,"month":5,"date":22},{"name":"寛徳","year":1044,"month":12,"date":16},{"name":"長久","year":1040,"month":12,"date":16},{"name":"長暦","year":1037,"month":5,"date":9},{"name":"長元","year":1028,"month":8,"date":18},{"name":"万寿","year":1024,"month":8,"date":19},{"name":"治安","year":1021,"month":3,"date":17},{"name":"寛仁","year":1017,"month":5,"date":21},{"name":"長和","year":1013,"month":2,"date":8},{"name":"寛弘","year":1004,"month":8,"date":8},{"name":"長保","year":999,"month":2,"date":1},{"name":"長徳","year":995,"month":3,"date":25},{"name":"正暦","year":990,"month":11,"date":26},{"name":"永祚","year":989,"month":9,"date":10},{"name":"永延","year":987,"month":5,"date":5},{"name":"寛和","year":985,"month":5,"date":19},{"name":"永観","year":983,"month":5,"date":29},{"name":"天元","year":978,"month":12,"date":31},{"name":"貞元","year":976,"month":8,"date":11},{"name":"天延","year":974,"month":1,"date":16},{"name":"天禄","year":970,"month":5,"date":3},{"name":"安和","year":968,"month":9,"date":8},{"name":"康保","year":964,"month":8,"date":19},{"name":"応和","year":961,"month":3,"date":5},{"name":"天徳","year":957,"month":11,"date":21},{"name":"天暦","year":947,"month":5,"date":15},{"name":"天慶","year":938,"month":6,"date":22},{"name":"承平","year":931,"month":5,"date":16},{"name":"延長","year":923,"month":5,"date":29},{"name":"延喜","year":901,"month":8,"date":31},{"name":"昌泰","year":898,"month":5,"date":20},{"name":"寛平","year":889,"month":5,"date":30},{"name":"仁和","year":885,"month":3,"date":11},{"name":"元慶","year":877,"month":6,"date":1},{"name":"貞観","year":859,"month":5,"date":20},{"name":"天安","year":857,"month":3,"date":20},{"name":"斉衡","year":854,"month":12,"date":23},{"name":"仁寿","year":851,"month":6,"date":1},{"name":"嘉祥","year":848,"month":7,"date":16},{"name":"承和","year":834,"month":2,"date":14},{"name":"天長","year":824,"month":2,"date":8},{"name":"弘仁","year":810,"month":10,"date":20},{"name":"大同","year":806,"month":6,"date":8},{"name":"延暦","year":782,"month":9,"date":30},{"name":"天応","year":781,"month":1,"date":30},{"name":"宝亀","year":770,"month":10,"date":23},{"name":"神護景雲","year":767,"month":9,"date":13},{"name":"天平神護","year":765,"month":2,"date":1},{"name":"天平宝字","year":757,"month":9,"date":6},{"name":"天平勝宝","year":749,"month":8,"date":19},{"name":"天平感宝","year":749,"month":5,"date":4},{"name":"天平","year":729,"month":9,"date":2},{"name":"神亀","year":724,"month":3,"date":3},{"name":"養老","year":717,"month":12,"date":24},{"name":"霊亀","year":715,"month":10,"date":3},{"name":"和銅","year":708,"month":2,"date":7},{"name":"慶雲","year":704,"month":6,"date":16},{"name":"大宝","year":701,"month":5,"date":3},{"name":"－","year":686,"month":10,"date":1},{"name":"朱鳥","year":686,"month":8,"date":14},{"name":"－","year":654,"month":11,"date":24},{"name":"白雉","year":650,"month":3,"date":22},{"name":"大化","year":645,"month":7,"date":17},{"name":"平成","year":1989,"month":1,"date":8}];

		var year = d.getFullYear( );
		var month = d.getMonth( ) + 1;
		var date = d.getDate( );
		var pack = ( year * 100 + month ) * 100 + date;

		for ( t in table ) {
			if ( ( t.year * 100 + t.month ) * 100 + t.date <= pack ) {
				var md = "" + month + "月" + date + "日";
				return if ( year == t.year ) {
					t.name + "元年" + md;
				}else {
					t.name + ( year - t.year + 1 ) + "年" + md;
				};
			}
		}

		return null;
	}

	/**
	 * 皇歴に変換する
	 * @param	y	西暦
	 * @return	皇歴
	 */
	static public function getJapaneseImperialYear( y:Int ):Int
	{
		return y + 660;
	}

	/**
	 * ユリウス日を取得
	 * @param	d
	 * @return
	 */
	static public function getJulianDate( d:Date ):Float
	{
		return 2440587.5 + d.getTime( ) / 86400000;
	}

	/**
	 * ユリウス日からDateを作成
	 * @param	jd
	 * @return
	 */
	static public function makeJulianDate( jd:Float ):Date
	{
		return Date.fromTime( ( jd - 2440587.5 ) * 86400000 );
	}

	static private var loaded = false;
	static private var dayFixedTable:Map<Int,Array<DateFixedColumn>>;
	static private var weekFixedTable:Array<WeekFixedColumn>;

	/**
	 * データ読み込み
	 */
	static private function loadData( )
	{
		if ( DateUtil.loaded ) return;
		DateUtil.loaded = true;

		// 日付固定の日データ
		DateUtil.dayFixedTable = new Map<Int,Array<DateFixedColumn>>( );
		var table:Array<DateFixedColumn> = Json.parse( CompileTime.readJsonFile( "./data/date.json" ) );
		for ( t in table ) {
			var name = Std.string( t.name );
			var start = if ( t.start == null ) {
				null;
			}else {
				Std.parseInt( Std.string( t.start ) );
			};
			var expired = if ( t.expired == null ) {
				null;
			}else {
				Std.parseInt( Std.string( t.expired ) );
			};
			var month = Std.parseInt( Std.string( t.month ) );
			var date = Std.parseInt( Std.string( t.date ) );
			var md = month * 100 + date;
			if( ! DateUtil.dayFixedTable.exists( md ) ) {
				DateUtil.dayFixedTable.set( md, new Array<DateFixedColumn>( ) );
			}
			DateUtil.dayFixedTable.get( md ).push({
				name: name,
				start: start,
				expired: expired,
				month: month,
				date: date,
			});
		}
		// 週と曜日固定の日付データ
		DateUtil.weekFixedTable = new Array<WeekFixedColumn>( );
		var table:Array<WeekFixedColumn> = Json.parse( CompileTime.readJsonFile( "./data/week.json" ) );
		for( t in table ) {
			var name = Std.string( t.name );
			var start = if ( t.start == null ) {
				null;
			}else {
				Std.parseInt( Std.string( t.start ) );
			};
			var expired = if ( t.expired == null ) {
				null;
			}else {
				Std.parseInt( Std.string( t.expired ) );
			};
			var month = Std.parseInt( Std.string( t.month ) );
			var week = Std.parseInt( Std.string( t.week ) );
			var day = Std.parseInt( Std.string( t.day ) );
			DateUtil.weekFixedTable.push({
				name: name,
				start: start,
				expired: expired,
				month: month,
				week: week,
				day: day,
			});
		}
	}

	/**
	 * 日付から名前取得
	 * @param	d	日付
	 * @return	日付の情報
	 */
	static public function getDateInfo( d:Date ):DateInfo
	{
		DateUtil.loadData( );

		var name = "";
		var otherNames = new Array<String>( );
		var flags = new EnumFlags<DateInfoFlag>( );
		var year = d.getFullYear( );
		var month = d.getMonth( ) + 1;
		var date = d.getDate( );
		var week = Math.floor( ( date + 6 ) / 7 );
		var day = d.getDay( );
		var md = month * 100 + date;
		var ymd = year * 10000 + md;

		if( ! DateUtil.dayFixedTable.exists( md ) ) {
			// 振替休日
			if( 19730412 <= ymd ) {
				if ( day == 1 ) {
					if ( DateUtil.dayFixedTable.exists( md - 1 ) ) {
						for( t in DateUtil.dayFixedTable.get( md - 1 ) ) {
							if( t.start != null ) {
								if ( ymd < t.start ) continue;
							}
							if( t.expired != null ) {
								if ( t.expired <= ymd ) continue;
							}
							name = t.name;
							flags.set( DateInfoFlag.holiday );
							flags.set( DateInfoFlag.substituteHoliday );
							break;
						}
					}
				}
			}
		}else {
			// 当日
			for( t in DateUtil.dayFixedTable.get( md ) ) {
				otherNames.push( t.name );
				if( t.start != null ) {
					if ( ymd < t.start ) continue;
				}
				if( t.expired != null ) {
					if ( t.expired <= ymd ) continue;
				}
				// 日曜から月曜へ移動
				if( 19730412 <= ymd ) {
					if ( day == 0 ) continue;
				}
				name = t.name;
				flags.set( DateInfoFlag.holiday );
			}
		}

		// 第n週目のy曜日指定
		for ( t in DateUtil.weekFixedTable ) {
			if ( t.month != month ) continue;
			if ( t.week != week ) continue;
			if ( t.day != day ) continue;
			otherNames.push( t.name );
			if( t.start != null ) {
				if ( ymd < t.start ) continue;
			}
			if( t.expired != null ) {
				if ( t.expired <= ymd ) continue;
			}
			name = t.name;
			flags.set( DateInfoFlag.holiday );
		}

		// 両側が休日だから休日化
		if ( ( 19851227 <= ymd )&&( ! flags.has( DateInfoFlag.holiday ) ) ) {
			// 前日が休日か？
			var yesterday = if ( day == 1 ) {
				true;
			}else if ( DateUtil.dayFixedTable.exists( md - 1 ) ) {
				var found = false;
				for( t in DateUtil.dayFixedTable.get( md - 1 ) ) {
					if( t.start != null ) {
						if ( ymd < t.start ) continue;
					}
					if( t.expired != null ) {
						if ( t.expired <= ymd ) continue;
					}
					found = true;
					break;
				}
				found;
			}else {
				false;
			};
			// 次の日が休日か？
			var tomorrow = if( day == 5 ) {
				true;
			}else if ( DateUtil.dayFixedTable.exists( md + 1 ) ) {
				var found = false;
				for( t in DateUtil.dayFixedTable.get( md + 1 ) ) {
					if( t.start != null ) {
						if ( ymd < t.start ) continue;
					}
					if( t.expired != null ) {
						if ( t.expired <= ymd ) continue;
					}
					found = true;
					break;
				}
				found;
			}else {
				false;
			};
			if( yesterday && tomorrow ) {
				flags.set( DateInfoFlag.holiday );
				flags.set( DateInfoFlag.changeToHoliday );
				name = "国民の休日";
			}
		}

		// 閏年
		if( ( month == 2 )&&( date == 29 ) ) {
			flags.set( DateInfoFlag.leapYear );
		}

		return {
			d: d,
			name: name,
			otherNames: otherNames,
			flags: flags,
		};
	}
}
