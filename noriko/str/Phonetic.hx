package noriko.str;

/**
 * のりこ - Phonetic アルゴリズム
 * @author あるる（きのもと 結衣）
 */
class Phonetic
{
	/**
	 * Soundexアルゴリズムに基づいて単語を変換する
	 * @param	a	元単語
	 * @return
	 */
	static public function soundex( a:String ):String
	{
		var s = a.toUpperCase( );
		var result = s.charAt(0);
		var lastCode = "";

		for ( char in s.split("") ) {
			var code = switch( char ) {
				case "B", "F", "P", "V":
					"1";
				case "C", "G", "J", "K", "Q", "S", "X", "Z":
					"2";
				case "D", "T":
					"3";
				case "L":
					"4";
				case "M", "N":
					"5";
				case "R":
					"6";
				default:
					"";
			};

			if ( ( code != "" ) && ( lastCode != code ) ) {
				lastCode = code;
				result += code;
				if ( 4 < result.length ) break;
			}
		}


		return ( result + "0000" ).substr(0,4);
	}

	/**
	 * Caverphoneアルゴリズムに基づいて単語を変換する
	 * @param	a	元単語
	 * @return
	 */
	static public function caverphone( a:String ):String
	{
		if ( a.length == 0 ) return "1111111111";

		var s = a.toLowerCase( );
		var list = [
			// 英語小文字以外は削除
			{ from: "[^a-z]", to: "" },

			// 最後のeを削除
			{ from: "e$", to: "" },

			// 先頭から始まるもの
			{ from: "^cough", to: "cou2f" },
			{ from: "^rough", to: "rou2f" },
			{ from: "^tough", to: "tou2f" },
			{ from: "^enough", to: "enou2f" },
			{ from: "^trough", to: "trou2f" },
			{ from: "^gn", to: "2n" },
			{ from: "^mb", to: "m2" },

			// 置換
			{ from: "cq", to: "2q" },
			{ from: "ci", to: "si" },
			{ from: "ce", to: "se" },
			{ from: "cy", to: "sy" },
			{ from: "tch", to: "2ch" },
			{ from: "c", to: "k" },
			{ from: "q", to: "k" },
			{ from: "x", to: "k" },
			{ from: "v", to: "f" },
			{ from: "dg", to: "2g" },
			{ from: "tio", to: "sio" },
			{ from: "tia", to: "sia" },
			{ from: "d", to: "t" },
			{ from: "ph", to: "fh" },
			{ from: "b", to: "p" },
			{ from: "sh", to: "s2" },
			{ from: "z", to: "s" },
			{ from: "^[aeiou]", to: "A" },
			{ from: "[aeiou]", to: "3" },
			{ from: "j", to: "y" },
			{ from: "^y3", to: "Y3" },
			{ from: "^y", to: "A" },
			{ from: "y", to: "3" },
			{ from: "3gh3", to: "3kh3" },
			{ from: "gh", to: "22" },
			{ from: "g", to: "k" },
			{ from: "s+", to: "S" },
			{ from: "t+", to: "T" },
			{ from: "p+", to: "P" },
			{ from: "k+", to: "K" },
			{ from: "f+", to: "F" },
			{ from: "m+", to: "M" },
			{ from: "n+", to: "N" },
			{ from: "w3", to: "W3" },
			{ from: "wh3", to: "Wh3" },
			{ from: "w$", to: "3" },
			{ from: "w", to: "2" },
			{ from: "^h", to: "A" },
			{ from: "h", to: "2" },
			{ from: "r3", to: "R3" },
			{ from: "r$", to: "3" },
			{ from: "r", to: "2" },
			{ from: "l3", to: "L3" },
			{ from: "l$", to: "3" },
			{ from: "l", to: "2" },

			// ハンドル削除
			{ from: "2", to: "" },
			{ from: "3$", to: "A" },
			{ from: "3", to: "" },
		];
		for ( t in list ) {
			s = new EReg( t.from, "g" ).replace( s, t.to );
		}

		return ( s + "1111111111" ).substr( 0, 10 );
	}
}