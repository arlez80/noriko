package noriko.str;

using unifill.Unifill;
using Lambda;

/**
 * コールバック式 CSVリーダー
 */
class CSV
{
	static public var defaultSplitter = ",";
	static public var defaultQuote = "\"";
	static public var eol( default, never ) = [ "\r", "\n" ];

	/**
	 * 読み込む
	 * @param	data
	 * @param	cb
	 */
	static public function reader( data:String, cb:Int->Array<String>->Void ):Void
	{
		var csv = new CSV( data, cb );
		csv.read( );
	}

	/**
	 * ヘッダー付きで読み込む
	 * @param	data
	 * @param	cb
	 */
	static public function readerWithHeader( data:String, cb:Int->Map<String,String>->Void ):Void
	{
		var header = null;
		var csv = new CSV( data, function( id:Int, line:Array<String> ) {
			if ( header == null ) {
				header = line;
				return;
			}
			var map = new Map<String,String>( );
			var i:Int = 0;
			for ( t in header ) {
				map.set( t, line[i] );
				i ++;
			}
			cb( id, map );
		});
		csv.read( );
	}

	public var splitter:String;
	public var quote:String;
	public var data:String;
	public var cb:Int->Array<String>->Void;

	/**
	 * コンストラクタ
	 * @param	data	CSVデータ
	 * @param	cb		コールバック
	 */
	public function new( data:String, cb:Int->Array<String>->Void ):Void
	{
		this.data = data;
		this.cb = cb;
		this.splitter = CSV.defaultSplitter;
		this.quote = CSV.defaultQuote;
	}

	/**
	 * CSV読み込み
	 */
	public function read( ):Void
	{
		var i:Int = 0;
		var len:Int = this.data.length;
		var counter:Int = 0;

		while ( i < len ) {
			var line = new Array<String>( );
			var buf = "";
			var fQuote = false;

			while ( i < len ) {
				// trace( buf );
				var c = this.data.uCharAt(i); i ++;
				if ( eol.exists( function(a) { return a == c; } ) ) break;

				if ( c == this.quote ) {
					if( this.data.uCharAt(i) == this.quote ) {
						buf += c;
						i ++;
					}else {
						fQuote = ! fQuote;
					}
				}else if( c == this.splitter ) {
					if ( ! fQuote ) {
						line.push( buf );
						buf = "";
					}else {
						buf += c;
					}
				}else {
					buf += c;
				}
			}

			if( buf != "" ) line.push( buf );

			if( 0 < line.length ) {
				cb( counter, line );
				counter ++;
			}
		}
	}
}
