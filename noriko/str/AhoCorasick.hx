package noriko.str;

import noriko.ds.Trie;

using unifill.Unifill;

/**
 * エイホ-コラシック法を用いて文字列検索を行う
 * @author あるる（きのもと 結衣）
 */
class AhoCorasick
{
	/// ノード
	public var words:Trie;

	/**
	 * コンストラクタ
	 * @param	words	単語一覧
	 */
	public function new( ?words:Trie )
	{
		this.words = ( words != null ) ? words : new Trie( );
	}

	/**
	 * マッチ
	 * @param	src	検索元文字列
	 * @return	マッチした文字列
	 */
	public function match( src:String ):Array<String>
	{
		var s = src.uSplit( "" );
		var results = new Array<String>( );

		for ( i in 0 ... s.length ) {
			var k = i;
			var node = this.words;

			while ( k < s.length ) {
				var c = s[k];
				if ( node.has( c ) ) {
					node = node.get( c );
					if ( node.value != null ) {
						results.push( node.value );
					}
				}else {
					break;
				}
	
				k ++;
			}
		}

		return results;
	}
}
