package noriko.str;

import noriko.ds.IEEE754Util;
import unifill.InternalEncoding;

using unifill.Unifill;

typedef FormatFlag = {
	id:Null<Int>,		// %<number>$のnumber

	left:Bool,			// -
	sign:Bool,			// +
	zero:Bool,			// 0
	// alter:			// #

	digit:Null<Int>,	// 桁
	frac:Null<Int>,		// 小数部

	to:String,			// 変換後
};

/**
 * のりこ - 文字列関連
 * @author あるる（きのもと 結衣）
 */
class JString
{
	/// ひらがな
	static private var hiragana = "ぁあぃいぅうぇえぉおかがきぎくぐけげこごさざしじすずせぜそぞただちぢっつづてでとどなにぬねのはばぱひびぴふぶぷへべぺほぼぽまみむめもゃやゅゆょよらりるれろゎわゐゑをん";
	/// カタカナ
	static private var katakana = "ァアィイゥウェエォオカガキギクグケゲコゴサザシジスズセゼソゾタダチヂッツヅテデトドナニヌネノハバパヒビピフブプヘベペホボポマミムメモャヤュユョヨラリルレロヮワヰヱヲンヴヵヶ";
	/// 半角カナ
	static private var halfKana = "ｱｲｳｴｵｶｷｸｹｺｻｼｽｾｿﾀﾁﾂﾃﾄﾅﾆﾇﾈﾉﾊﾋﾌﾍﾎﾏﾐﾑﾒﾓﾔﾕﾖﾗﾘﾙﾚﾛﾜｦﾝｧｨｩｪｫｬｭｮｯｰﾞﾟ";

	/**
	 * アルファベットか判定
	 * @param	s
	 * @return
	 */
	static public function isAlpha( s:String ):Bool
	{
		var c = s.uCharCodeAt( 0 );
		return (
			( ( "A".code <= c )&&( c <= "Z".code ) ) ||		// 大文字
			( ( "a".code <= c )&&( c <= "z".code ) )		// 小文字
		);
	}

	/**
	 * ウムラウト付きの文字か否かを判定
	 * @param	s
	 * @return
	 */
	static public function isUmlautChar( s:String ):Bool
	{
		var c = s.uCharAt( 0 );
		return (
			( "Ä" == c )
		||	( "ä" == c )
		||	( "Ï" == c )
		||	( "ï" == c )
		||	( "Ö" == c )
		||	( "ö" == c )
		||	( "Ü" == c )
		||	( "ü" == c )
		);
	}

	/**
	 * 数字か判定
	 * @param	s
	 * @return
	 */
	static public function isNumber( s:String ):Bool
	{
		var c = s.uCharCodeAt( 0 );
		return ( "0".code <= c ) && ( c <= "9".code );
	}

	/**
	 * ひらがなか判定
	 * @param	s
	 * @return
	 */
	static public function isHiragana( s:String ):Bool
	{
		var c = s.uCharAt( 0 );
		return JString.hiragana.uIndexOf( c ) != -1;
	}

	/**
	 * カタカナか判定
	 * @param	s
	 * @return
	 */
	static public function isKatakana( s:String ):Bool
	{
		var c = s.uCharAt( 0 );
		return JString.katakana.uIndexOf( c ) != -1;
	}

	/**
	 * 半角カタカナか判定
	 * @param	s
	 * @return
	 */
	static public function isHalfKana( s:String ):Bool
	{
		var c = s.uCharAt( 0 );
		return JString.halfKana.uIndexOf( c ) != -1;
	}

	/**
	 * 漢字か判定
	 * @param	s
	 * @return
	 */
	static public function isKanji( s:String ):Bool
	{
		var c = s.uCharCodeAt( 0 );
		return 19968 <= c && c <= 40864;
	}

	/**
	 * ひらがなをカタカナに変換
	 * @param	s
	 * @return
	 */
	static public function toKatakana( s:String ):String
	{
		var hira = JString.hiragana.split( "" );
		var kata = JString.katakana.split( "" );

		for ( i in 0 ... hira.length ) {
			s = s.split( hira[i] ).join( kata[i] );
		}

		return s;
	}

	/**
	 * カタカナをひらがなに変換
	 * @param	s
	 * @return
	 */
	static public function toHiragana( s:String ):String
	{
		var hira = JString.hiragana.split( "" );
		var kata = JString.katakana.split( "" );

		for ( i in 0 ... hira.length ) {
			s = s.split( kata[i] ).join( hira[i] );
		}

		return s;
	}

	/**
	 * ローマ字に変換する
	 * @param s
	 * @return
	 */
	static public function kanaToRomaji( s:String ):String
	{
		var table = [
			"シャ" => "sha", "シュ" => "shu", "シェ" => "she", "ショ" => "sho",
			"ジャ" => "ja", "ジュ" => "ju", "ジェ" => "je", "ジョ" => "jo",
			"チャ" => "cha", "チュ" => "chu", "チェ" => "che", "チョ" => "cho",
			"ヂャ" => "dya", "ヂュ" => "dyu", "ヂェ" => "dye", "ヂョ" => "dyo",
			"ティ" => "thi",
			"デャ" => "dha", "ディ" => "dhi", "デュ" => "dhu", "デェ" => "dhe", "デョ" => "dho",
			"ヒャ" => "hya", "ヒィ" => "hyi", "ヒュ" => "hyu", "ヒェ" => "hye", "ヒョ" => "hyo",
			"ビャ" => "bya", "ビィ" => "byi", "ビュ" => "byu", "ビェ" => "bye", "ビョ" => "byo",
			"ピャ" => "pya", "ピィ" => "pyi", "ピュ" => "pyu", "ピェ" => "pye", "ピョ" => "pyo",
			"ファ" => "fa", "フィ" => "fi", "フェ" => "fe", "フォ" => "fo", "フャ" => "fya", "フュ" => "fyu", "フョ" => "fyo", 
			"リャ" => "rya", "リィ" => "ryi", "リュ" => "ryu", "リェ" => "rye", "リョ" => "ryo",
			"ヴァ" => "va", "ヴィ" => "vi", "ヴェ" => "ve", "ヴォ" => "vo",
			"ァ" => "xa","ィ"=>"xi","ゥ"=>"xu","ェ"=>"xe","ォ"=>"xo",
			"ア" => "a", "イ" => "i", "ウ" => "u", "エ" => "e", "オ" => "o",
			"カ" => "ka", "ガ" => "ga", "キ" => "ki", "ギ" => "gi", "ク" => "ku", "グ" => "gu", "ケ" => "ke", "ゲ" => "ge", "コ" => "ko", "ゴ" => "go",
			"サ" => "sa", "ザ" => "za", "シ" => "si", "ジ" => "zi", "ス" => "su", "ズ" => "zu", "セ" => "se", "ゼ" => "ze", "ソ" => "so", "ゾ" => "zo",
			"タ" => "ta", "ダ" => "da", "チ" => "ti", "ヂ" => "di", "ツ" => "tu", "ヅ" => "du", "テ" => "te", "デ" => "de", "ト" => "to", "ド" => "do",
			"ナ" => "na", "ニ" => "ni", "ヌ" => "nu", "ネ" => "ne", "ノ" => "no",
			"ハ" => "ha", "バ" => "ba", "パ" => "pa", "ヒ" => "hi", "ビ" => "bi", "ピ" => "pi", "フ" => "hu", "ブ" => "bu", "プ" => "pu", "ヘ" => "he", "ベ" => "be", "ペ" => "pe", "ホ" => "ho", "ボ" => "bo", "ポ" => "po",
			"マ" => "ma", "ミ" => "mi", "ム" => "mu", "メ" => "me", "モ" => "mo",
			"ャ" => "xya", "ュ" => "xyu", "ョ" => "xyo",
			"ヤ" => "ya", "ユ" => "yu", "ヨ" => "yo",
			"ラ" => "ra", "リ" => "ri", "ル" => "ru", "レ" => "re", "ロ" => "ro",
			"ヮ" => "xwa","ヰ"=>"wi","ヱ"=>"xwe",
			"ワ" => "wa", "ヲ" => "wo", "ン" => "nn", "ヴ" => "vu",
			"ヵ" => "xka", "ヶ" => "xke",
			"ー" => "-", "！" => "!", "？" => "?",
		];

		var result = new StringBuf( );
		// 「ッ」フラグ
		var flagXtu = false;
		var i:Int = 0;
		var length = s.uLength( );

		while ( i < length ) {
			var c = s.uCharAt( i );
			var c2 = if( i < length - 1 ) s.uSubstr( i, 2 ) else "";
			if ( c == "ッ" ) {
				if ( flagXtu ) {
					result.add( "xtu" );
				}
				flagXtu = true;
			}else {
				if ( table.exists( c2 ) ) {
					var romaji = table.get( c2 );
					if ( flagXtu ) {
						romaji = romaji.substr( 0, 1 ) + romaji;
					}
					result.add( romaji );
					i ++;
				}else if ( table.exists( c ) ) {
					var romaji = table.get( c );
					if ( flagXtu ) {
						romaji = romaji.substr( 0, 1 ) + romaji;
					}
					result.add( romaji );
				}else {
					if ( flagXtu ) {
						result.add( "xtu" );
					}
					result.add( c );
				}
				flagXtu = false;
			}

			i ++;
		}

		return result.toString( );
	}

	/**
	 * ローマ字をかなに変換
	 * @param	a
	 * @param	b
	 * @return
	 */
	static public function romajiToKana( s:String ):String
	{
		var table = [
			"k" => [ "a" => "カ", "i" => "キ", "u" => "ク", "e" => "ケ", "o" => "コ" ],
			"g" => [ "a" => "ガ", "i" => "ギ", "u" => "グ", "e" => "ゲ", "o" => "ゴ" ],
			"s" => [ "a" => "サ", "i" => "シ", "u" => "ス", "e" => "セ", "o" => "ソ" ],
			"z" => [ "a" => "ザ", "i" => "ジ", "u" => "ズ", "e" => "ゼ", "o" => "ゾ" ],
			"t" => [ "a" => "タ", "i" => "チ", "u" => "ツ", "e" => "テ", "o" => "ト" ],
			"d" => [ "a" => "ダ", "i" => "ヂ", "u" => "ヅ", "e" => "デ", "o" => "ド" ],
			"n" => [ "a" => "ナ", "i" => "ニ", "u" => "ヌ", "e" => "ネ", "o" => "ノ" ],
			"h" => [ "a" => "ハ", "i" => "ヒ", "u" => "フ", "e" => "ヘ", "o" => "ホ" ],
			"b" => [ "a" => "バ", "i" => "ビ", "u" => "ブ", "e" => "ベ", "o" => "ボ" ],
			"p" => [ "a" => "パ", "i" => "ピ", "u" => "プ", "e" => "ペ", "o" => "ポ" ],
			"m" => [ "a" => "マ", "i" => "ミ", "u" => "ム", "e" => "メ", "o" => "モ" ],
			"y" => [ "a" => "ヤ", "i" => "ィ", "u" => "ゥ", "e" => "イェ", "o" => "ヨ" ],
			"r" => [ "a" => "ラ", "i" => "リ", "u" => "ル", "e" => "レ", "o" => "ロ" ],
			"w" => [ "a" => "ワ", "i" => "ヰ", "u" => "ゥ", "e" => "ヱ", "o" => "ヲ" ],

			"f" => [ "a" => "ファ", "i" => "フィ", "u" => "フ", "e" => "フェ", "o" => "フォ" ],
			"c" => [ "a" => "キャ", "i" => "シ", "u" => "キュ", "e" => "セ", "o" => "コ" ],
			"j" => [ "a" => "ジャ", "i" => "ジ", "u" => "ジュ", "e" => "ジェ", "o" => "ジョ" ],
			"v" => [ "a" => "ヴァ", "i" => "ヴィ", "u" => "ヴ", "e" => "ヴェ", "o" => "ヴォ" ],
			"x" => [ "a" => "ァ", "i" => "ィ", "u" => "ゥ", "e" => "ェ", "o" => "ォ" ],

			"sh" => [ "a" => "シャ", "i" => "シ", "u" => "シュ", "e" => "シェ", "o" => "ショ" ],
			"sy" => [ "a" => "シャ", "i" => "シ", "u" => "シュ", "e" => "シェ", "o" => "ショ" ],
			"ty" => [ "a" => "チャ", "i" => "チィ", "u" => "チュ", "e" => "チェ", "o" => "チョ" ],
			"ch" => [ "a" => "チャ", "i" => "チ", "u" => "チュ", "e" => "チェ", "o" => "チョ" ],
			"hy" => [ "a" => "ヒャ", "i" => "ヒィ", "u" => "ヒュ", "e" => "ヒェ", "o" => "ヒョ" ],
			"by" => [ "a" => "ビャ", "i" => "ビィ", "u" => "ビュ", "e" => "ビェ", "o" => "ビョ" ],
			"py" => [ "a" => "ピャ", "i" => "ピィ", "u" => "ピュ", "e" => "ピェ", "o" => "ピョ" ],
			"ry" => [ "a" => "リャ", "i" => "リィ", "u" => "リュ", "e" => "リェ", "o" => "リョ" ],

			"xy" => [ "a" => "ャ", "i" => "ィ", "u" => "ュ", "e" => "ェ", "o" => "ョ" ],
			"xk" => [ "a" => "ヵ", "e" => "ヶ" ],
		];

		var src = s.toLowerCase().split( "" );
		var result = new StringBuf( );
		var lastChar = "";

		for ( c in src ) {
			if ( ! JString.isAlpha( c ) ) {
				if ( lastChar != "" ) {
					result.add( lastChar );
				}
				result.add( c );
				continue;
			}
			if ( lastChar == "" ) {
				switch( c ) {
					case "a": result.add("ア");
					case "i": result.add("イ");
					case "u": result.add("ウ");
					case "e": result.add("エ");
					case "o": result.add("オ");
					default: lastChar = c;
				}
				continue;
			}
			if ( lastChar == c ) {
				result.add("ッ");
				continue;
			}

			if ( table.exists( lastChar ) ) {
				var target = table.get( lastChar );
				if ( target.exists( c ) ) {
					result.add( target.get( c ) );
					lastChar = "";
				}else {
					if ( lastChar.length == 1 ) {
						if ( lastChar == "n" ) {
							result.add( "ン" );
							if ( c == "n" ) {
								lastChar = "";
							}else {
								lastChar = c;
							}
						}else {
							lastChar = lastChar + c;
						}
					}else {
						result.add( lastChar );
						lastChar = c;
					}
				}
			}
		}

		switch( lastChar ) {
			case "":
			case "n": result.add( "ン" );
			default: result.add( lastChar );
		}

		return result.toString( );
	}

	/**
	 * リーベンシュタイン距離を取得
	 * @param	a	文字列A
	 * @param	b	文字列B
	 * @return	距離
	 */
	static public function levenshtein( a:String, b:String ):Int
	{
		var al = a.uLength( );
		var bl = b.uLength( );
		var d = new Array<Array<Int>>( );

		for ( ai in 0 ... al + 1 ) {
			d[ai] = new Array<Int>( );
			d[ai][0] = ai;
		}
		for ( bi in 0 ... bl + 1 ) {
			d[0][bi] = bi;
		}

		for ( ai in 1 ... al + 1 ) {
			for ( bi in 1 ... bl + 1 ) {
				var cost = if ( a.uCharAt( ai - 1 ) == b.uCharAt( bi - 1 ) ) 0 else 1;
				d[ai][bi] = Math.floor( Math.min( Math.min( d[ai - 1][bi] + 1, d[ai][bi - 1] + 1), d[ai - 1][bi - 1] + cost ) );
			}
		}

		return d[al][bl];
	}

	/**
	 * 簡易住所分割
	 * 
	 * @param	addr	住所
	 * @return	住所を{ pref:都道府県, city:市区町村, detail:それ以降 }に分割する。マッチしなければnullを返す
	 * @see 参考URL http://qiita.com/zakuroishikuro/items/066421bce820e3c73ce9
	 */
	static public function splitAddress( addr:String ):{ pref:String, city:String, detail:String }
	{
		// 空白除去
		addr = addr.split( " " ).join( "" ).split( "　" ).join( "" );

		// 分割
		var reg = new EReg( "(...??[都道府県])((?:旭川|伊達|石狩|盛岡|奥州|田村|南相馬|那須塩原|東村山|武蔵村山|羽村|十日町|上越|富山|野々市|大町|蒲郡|四日市|姫路|大和郡山|廿日市|下松|岩国|田川|大村)市|.+?郡(?:玉村|大町|.+?)[町村]|.+?市.+?区|.+?[市区町村])(.+)", "ug" );
		var result = { pref:"", city:"", detail:"" };
		try{
			reg.match( addr );
			result.pref = reg.matched( 1 );
			result.city = reg.matched( 2 );
			result.detail = reg.matched( 3 );
		}catch( d:Dynamic ) {
			result = null;
		};
		return result;
	}

	/**
	 * Cのstrcmpの実装
	 * @param	a
	 * @param	b
	 * @return
	 */
	static public function strcmp( a:String, b:String ):Int
	{
		var min = a.length < b.length ? a.length : b.length;
		return a.uCharCodeAt( min ) - b.uCharCodeAt( min );
	}

	/**
	 * 文字列繰り返しパターンを検出して返す
	 * @param	src	文字列
	 * @return	単語と繰り返し回数のリスト
	 */
	static public function findStringPattern( src:String ):Array<{word:String, count:Int}>
	{
		var re = #if ( cpp || neko ) ~/(.+?)\1+/gmu #else ~/(.+?)\1+/gm #end;
		var list = new Array<{word:String, count:Int}>( );

		while( 0 < src.length ) {
			if( re.match( src ) ) {
				var loop = re.matched( 0 );

				var loopStart = src.uIndexOf( loop );
				if ( 0 < loopStart ) {
					list.push( { word: src.uSubstr( 0, loopStart ), count: 1 } );
					src = src.uSubstr( loopStart );
				}

				var loopLength = loop.uLength( );
				var word = re.matched( 1 );
				list.push( { word: word, count: Math.floor( loopLength / word.uLength( ) ) } );
				src = src.uSubstr( loopLength );
			}else {
				// なし（残り全て）
				list.push( { word: src, count: 1 } );
				break;
			}
		}

		return list;
	}

	/**
	 * 書式指定変換
	 *
	 * c: Unicode 1文字(Int)
	 * d: 整数を10進数で(Int)
	 * u: （現在は上に同じ）
	 * o: 整数を8進数で(Int)
	 * x: 整数を小文字16進数で(Int)
	 * X: 整数を大文字16進数で(Int)
	 * f: 浮動小数点(Float)
	 * s: 文字列(String)
	 *
	 * @param	format
	 * @param	args
	 * @param	useStdString	floatに対する変換時にStd.Stringを用いるか否か。デフォルトfalse
	 * @return
	 */
	static public function sprintf( format:String, args:Array<Dynamic>, useStdString:Bool = false ):String
	{
		var getFlags = function( s:String ):FormatFlag {
			var result = {
				id: null,

				left: false,
				sign: false,
				zero: false,

				digit: null,
				frac: null,

				to: "s",
			};
			var doller = s.uIndexOf( "$" );
			result.id = if ( doller != -1 ) {
				Std.parseInt( s.uSubstr( 1, doller ) ) - 1;
			}else {
				null;
			};

			var i:Int = if( doller != -1 ) doller else 1;
			while ( i < s.uLength( ) ) {
				var c = s.uCharAt(i);
				switch( c ) {
					case "-": result.left = true;
					case "+": result.sign = true;
					case "0": result.zero = true;
					default:
						break;
				}
				i ++;
			}

			// 桁数
			var dot = s.uIndexOf( ".", i );
			result.digit = if ( dot != -1 ) {
				Std.parseInt( s.uSubstr( i, dot - i ) );
			}else {
				Std.parseInt( s.uSubstr( i ) );
			};
			if ( dot != -1 ) {
				result.frac = Std.parseInt( s.uSubstr( dot + 1 ) );
			}

			result.to = s.uSubstr( -1, 1 );

			return result;
		};

		var count:Int = 0;
		format = new EReg( "%([0-9]*\\$?\\-?\\+?0?[0-9]*\\.?[0-9]*[A-Za-z]|%)", "g" ).map( format, function( re ) {
			var flags:FormatFlag = getFlags( re.matched(0) );
			var id = if ( flags.id != null ) flags.id else count;
			count ++;

			return switch( flags.to ) {
				case "%":
					count --;
					"%";
				case "s":
					var str = Std.string( args[id] );
					if( flags.digit == null ) {
						str;
					}else {
						if ( flags.left ) {
							StringTools.lpad( str, " ", flags.digit );
						}else {
							StringTools.rpad( str, " ", flags.digit );
						};
					};
				case "d", "u", "o", "x", "X":
					var number = Std.parseInt( Std.string( args[id] ) );
					var str = switch( flags.to ) {
						case "d", "u": Std.string( number );
						case "o":
							var buf = "";
							do {
								buf = Std.string( number & 7 ) + buf;
								number = number >> 3;
							}while ( 0 < number );
							buf;
						case "x": StringTools.hex( number ).toLowerCase( );
						case "X": StringTools.hex( number );
						default: "0";
					};
					if ( ( flags.sign ) && ( str.substr( -1, 1 ) != "-" ) ) str = "+" + str;
					var padding = if ( flags.zero ) "0" else " ";
					if( flags.digit != null ) {
						str = if ( flags.left ) {
							StringTools.lpad( str, padding, flags.digit ).substr( 0, flags.digit );
						}else {
							StringTools.lpad( str, padding, flags.digit ).substr( -flags.digit );
						};
					}
					str;
				case "f":
					var str = if ( useStdString ) {
						Std.string( args[id] );
					}else {
						IEEE754Util.floatToString( args[id] );
					};
					if ( ( flags.sign ) && ( str.substr( -1, 1 ) != "-" ) ) str = "+" + str;
					if ( flags.frac != null ) {
						var numbers = str.split( "." );
						if ( 2 <= numbers.length ) {
							numbers[1] = StringTools.rpad( numbers[1], "0", flags.frac ).substr( 0, flags.frac );
							str = numbers.join( "." );
						}else {
							str = numbers[0] + "." + StringTools.rpad( "", "0", flags.frac );
						}
					}
					var padding = if ( flags.zero ) "0" else " ";
					if( flags.digit != null ) {
						str = if ( flags.left ) {
							StringTools.lpad( str, padding, flags.digit ).substr( 0, flags.digit );
						}else {
							StringTools.lpad( str, padding, flags.digit ).substr( -flags.digit );
						};
					}
					str;
				case "c":
					InternalEncoding.fromCodePoint( args[id] );
				default:
					// unknown or not implemented
					"";
			};
		} );

		return format;
	}
}
