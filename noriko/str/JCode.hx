package noriko.str;

import haxe.io.Bytes;
import haxe.io.BytesBuffer;
import haxe.io.BytesInput;
import haxe.zip.Reader;
import unifill.InternalEncoding;

using unifill.Unifill;

/**
 * のりこ - 文字列変換 ユーティリティ
 * @author あるる（きのもと 結衣）
 */
class JCode
{
#if java
	static public function toJIS( s:String ):String
	{
		return untyped __java__( "new java.lang.String( s.getBytes(\"ISO2022JP\"), \"ISO2022JP\" )" );
	}

	static public function fromJIS( s:String ):String
	{
		return untyped __java__( "new java.lang.String( s.getBytes(\"UTF16\"), \"UTF16\" )" );
	}

	static public function toSJIS( s:String ):String
	{
		return untyped __java__( "new java.lang.String( s.getBytes(\"Shift_JIS\"), \"Shift_JIS\" )" );
	}

	static public function fromSJIS( s:String ):String
	{
		return untyped __java__( "new java.lang.String( s.getBytes(\"UTF16\"), \"UTF16\" )" );
	}

	static public function toEUC( s:String ):String
	{
		return untyped __java__( "new java.lang.String( s.getBytes(\"EUC_JP\"), \"EUC_JP\" )" );
	}

	static public function fromEUC( s:String ):String
	{
		return untyped __java__( "new java.lang.String( s.getBytes(\"UTF16\"), \"UTF16\" )" );
	}

#else
	// その他の環境すべて

	static private var loaded:Bool = false;

	/// テーブル一覧
	/// 初めて使用する際に読み込む
	static private var jis2uni:Bytes;
	static private var uni2jis:Bytes;
	static private var sjis2uni:Bytes;
	static private var uni2sjis:Bytes;
	static private var euc2uni:Bytes;
	static private var uni2euc:Bytes;

	/**
	 * テーブルを読み込む
	 */
	static private function loadTables( ):Void
	{
		if ( JCode.loaded ) return;

		var data = Bytes.ofString( CompileTime.readFile( "./data/jcode.zip" ) );
		var zipData = new BytesInput( data );
		var zip = Reader.readZip( zipData );

		for ( entry in zip ) {
			switch( entry.fileName ) {
				case "jis2uni.txt": JCode.jis2uni = Reader.unzip( entry );
				case "uni2jis.txt": JCode.uni2jis = Reader.unzip( entry );
				case "sjis2uni.txt": JCode.sjis2uni = Reader.unzip( entry );
				case "uni2sjis.txt": JCode.uni2sjis = Reader.unzip( entry );
				case "euc2uni.txt": JCode.euc2uni = Reader.unzip( entry );
				case "uni2euc.txt": JCode.uni2euc = Reader.unzip( entry );
				default:
					// 無視
			}
		}

		JCode.loaded = true;
	}

	/**
	 * JISへ変換
	 * @param	s
	 * @return
	 */
	static public function toJIS( s:String ):String
	{
		JCode.loadTables( );

		var bb = new BytesBuffer( );
		var jmode = false;

		var i:Int = 0;
		var length = s.uLength();
		while ( i < length ) {
			var c = s.uCharCodeAt( i ); i ++;

			if( c < 0x80 ) {
				if ( jmode ) {
					bb.addByte( 0x1b );
					bb.addByte( 0x28 );
					bb.addByte( 0x42 );
					jmode = false;
				}
				bb.addByte( c );
			}else {
				if ( !jmode ) {
					bb.addByte( 0x1b );
					bb.addByte( 0x24 );
					bb.addByte( 0x42 );
					jmode = true;
				}
				bb.addByte( JCode.uni2jis.get( c * 2 ) );
				bb.addByte( JCode.uni2jis.get( c * 2 + 1 ) );
			}
		}

		// 日本語モードになっている場合は閉じる
		if ( jmode ) {
			bb.addByte( 0x1b );
			bb.addByte( 0x28 );
			bb.addByte( 0x42 );
		}

		return bb.getBytes( ).toString( );
	}

	/**
	 * JISから変換
	 * @param	s
	 * @return
	 */
	static public function fromJIS( s:String ):String
	{
		JCode.loadTables( );

		var src = Bytes.ofString( s );
		var buf = "";
		var mode = false;

		var i:Int = 0;
		var length = src.length;
		while ( i < length ) {
			var c = src.get(i); i ++;

			if ( c == 0x1b ) {
				var c = src.get(i); i ++;
				switch( c ) {
					case 0x28: mode = false;
					case 0x24: mode = true;
				}
				i ++;
				continue;
			}

			buf += if ( mode ) {
				var c2 = src.get(i); i ++;
				var code = c | ( c2 << 8 );
				var unicode = JCode.jis2uni.get( code * 2 ) | ( JCode.jis2uni.get( code * 2 + 1 ) << 8 );
				InternalEncoding.fromCodePoint( unicode );
			}else {
				InternalEncoding.fromCodePoint( c );
			};
		}

		return buf;
	}

	/**
	 * SJISへ変換
	 * @param	s
	 * @return
	 */
	static public function toSJIS( s:String ):String
	{
		JCode.loadTables( );

		var bb = new BytesBuffer( );

		var i:Int = 0;
		var length = s.uLength();
		while ( i < length ) {
			var c = s.uCharCodeAt( i ); i ++;

			if( c < 0x80 ) {
				bb.addByte( c );
			}else {
				bb.addByte( JCode.uni2sjis.get( c * 2 ) );
				bb.addByte( JCode.uni2sjis.get( c * 2 + 1 ) );
			}
		}

		return bb.getBytes( ).toString( );
	}

	/**
	 * SJISから変換
	 * @param	s
	 * @return
	 */
	static public function fromSJIS( s:String ):String
	{
		JCode.loadTables( );

		var src = Bytes.ofString( s );
		var buf = "";

		var i:Int = 0;
		var length = src.length;
		while ( i < length ) {
			var c = src.get( i ); i ++;

			buf += if( c < 0x80 ) {
				InternalEncoding.fromCodePoint( c );
			}else {
				var c2 = src.get( i ); i ++;
				var code = c | ( c2 << 8 );
				var unicode = JCode.sjis2uni.get( code * 2 ) | ( JCode.sjis2uni.get( code * 2 + 1 ) << 8 );
				InternalEncoding.fromCodePoint( unicode );
			};
		}

		return buf;
	}

	/**
	 * EUC-JPへ変換
	 * @param	s
	 * @return
	 */
	static public function toEUC( s:String ):String
	{
		JCode.loadTables( );

		var bb = new BytesBuffer( );

		var i:Int = 0;
		var length = s.uLength();
		while ( i < length ) {
			var c = s.uCharCodeAt( i ); i ++;

			if( c < 0x80 ) {
				bb.addByte( c );
			}else {
				bb.addByte( JCode.uni2euc.get( c * 2 ) );
				bb.addByte( JCode.uni2euc.get( c * 2 + 1 ) );
			}
		}

		return bb.getBytes( ).toString( );
	}

	/**
	 * EUC-JPから変換
	 * @param	s
	 * @return
	 */
	static public function fromEUC( s:String ):String
	{
		JCode.loadTables( );

		var src = Bytes.ofString( s );
		var buf = "";

		var i:Int = 0;
		var length = src.length;
		while ( i < length ) {
			var c = src.get( i ); i ++;

			buf += if( c < 0x80 ) {
				InternalEncoding.fromCodePoint( c );
			}else {
				var c2 = src.get( i ); i ++;
				var code = c | ( c2 << 8 );
				var unicode = JCode.euc2uni.get( code * 2 ) | ( JCode.euc2uni.get( code * 2 + 1 ) << 8 );
				InternalEncoding.fromCodePoint( unicode );
			};
		}

		return buf;
	}

#end
}
