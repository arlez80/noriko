package noriko.str;

using Lambda;

/**
 * のりこ - HTML解析
 * @author あるる（きのもと 結衣）
 */
class HTML
{
	// -----------------------------------
	// 非推奨
	@:deprecated static public function deleteElements( html:String ):String return HTML.deleteTags( html );
	// -----------------------------------

	/**
	 * タグ削除
	 * @param	html
	 * @return
	 */
	static public function deleteTags( html:String ):String
	{
		var re = ~/<("[^"]*"|'[^']*'|[^'">])*>/g;
		return re.replace( html, "" );
	}

	/**
	 * タグ名を取得
	 * @param	tag
	 * @return
	 */
	static public function getTagName( tag:String ):String
	{
		var re = ~/<("[^"]*"|'[^']*'|[^'">])*>/g;

		return if ( re.match( tag ) ) {
			var space = ~/\s/g;
			var s = re.matched( 0 );
			return space.split( s.substr( 1, s.length - 2 ) )[0];
		}else {
			"";
		};
	}

	/**
	 * エレメント取得
	 * @param	html	ソース
	 * @param	tag		切り出すタグ
	 * @return	切り出したタグを返す
	 */
	static public function getElements( html:String, tag:String ):Array<String>
	{
		var re = ~/<("[^"]*"|'[^']*'|[^'">])*>/g;
		var texts = re.split( html );
		var tags = new Array<String>( );

		re.map( html, function( re ):String {
			var t = re.matched(0);
			tags.push( t );
			return "";
		} );

		var hierarche:Int = 0;
		var copy = false;
		var result = new Array<String>( );
		var buffer = "";
		tag = tag.toLowerCase( );
		for ( i in 0 ... texts.length ) {
			if( copy ) {
				buffer += texts[i];
				if ( i < tags.length ) buffer += tags[i];
			}
			if ( i < tags.length ) {
				if ( tags[i].substr( 0, ( "<" + tag ).length )  == "<" + tag ) {
					if( !copy ) {
						buffer += tags[i];
					}
					hierarche ++;
					copy = true;
				}
				if ( tags[i].substr( 0, ( "</" + tag ).length ) == "</" + tag ) {
					hierarche --;
					if( hierarche <= 0 ) {
						copy = false;
						result.push( buffer );
						buffer = "";
					}
				}
			}
		}

		if( ( copy )&&( 0 < buffer.length ) ) {
			result.push( buffer );
		}

		return result;
	}

	/**
	 * エレメント取得
	 * @param	html	ソース
	 * @param	tag		切り出すタグ
	 * @return	切り出したタグを返す
	 */
	static public function getElementsByAttribute( html:String, attr:String ):Array<String>
	{
		var re = ~/<("[^"]*"|'[^']*'|[^'">])*>/g;
		var texts = re.split( html );
		var tags = new Array<String>( );

		re.map( html, function( re ):String {
			var t = re.matched(0);
			tags.push( t );
			return "";
		} );

		var result = new Array<String>( );
		for ( i in 1 ... texts.length - 1 ) {
			if ( HTML.getAttributes( tags[i] ).exists( attr ) ) {
				var tagName = HTML.getTagName( tags[i] );
				var buffer = "";
				var hierarche:Int = 0;
				for ( k in i ... texts.length - 1 ) {
					if ( k < tags.length ) {
						buffer += tags[k];
						if ( tags[k].substr( 0, ( "<" + tagName ).length )  == "<" + tagName ) {
							hierarche ++;
						}
						if ( tags[k].substr( 0, ( "</" + tagName ).length ) == "</" + tagName ) {
							hierarche --;
							if ( hierarche <= 0 ) break;
						}
					}

					buffer += texts[k + 1];
				}

				result.push( buffer );
			}
		}

		return result;
	}

	/**
	 * 階層エレメント切り出し
	 * @param	html	ソース
	 * @param	tags	タグの名前
	 * @return	切り出したタグを返す
	 */
	static public function getElementsByTree( html:String, tags:Array<String> ):Array<String>
	{
		var elements:Array<String> = [];
		var src = html;
		var iter = tags.iterator( );

		while ( iter.hasNext( ) ) {
			var tag = iter.next( );
			elements = HTML.getElements( src, tag );

			if ( ! iter.hasNext( ) ) break;

			src = elements.join( "" );
			if ( src == "" ) break;
		}

		return elements;
	}

	/**
	 * エレメントの中身を取得
	 * @param	html
	 * @return
	 */
	static public function getInnerHTML( html:String ):String
	{
		var re = ~/<("[^"]*"|'[^']*'|[^'">])*>/g;
		var texts = re.split( html );
		var tags = new Array<String>( );

		re.map( html, function( re ):String {
			var t = re.matched(0);
			tags.push( t );
			return "";
		} );

		var buffer = "";
		var hierarche = 0;
		for ( i in 1 ... texts.length - 1 ) {
			buffer += texts[i];
			if ( i < tags.length - 1 ) buffer += tags[i];
		}

		return buffer;
	}

	/**
	 * タグから属性を取得
	 * @param	src	元
	 * @return	取得した属性一覧。タグ自体がない場合はnullを返す
	 */
	static public function getAttributes( src:String ):Map<String, String>
	{
		var re = ~/<("[^"]*"|'[^']*'|[^'">])*>/g;
		if( ! re.match(src) ) return null;

		var tag = re.matched(0);
		tag = tag.substr( 0, tag.length-1 ) + " />";

		try {
			var xml = Xml.parse(tag).firstElement();
			var attr = new Map<String, String>( );
			for( key in xml.attributes() ) {
				attr.set( key, xml.get(key) );
			}

			return attr;
		}catch ( d:Dynamic ) {
			return new Map<String, String>( );
		}
	}

	/**
	 * スクリプトを取得
	 * @param	html
	 * @return
	 */
	static public function getScripts( html:String ):Array<String>
	{
		var re = ~/<script([\s\S]*?)>([\s\S]*?)<\/script>/g;

		var scripts = new Array<String>( );
		re.map( html, function( re ):String {
			scripts.push( re.matched(2) );
			return "";
		} );

		return scripts;
	}

	/**
	 * スクリプトを削除
	 * @param	html
	 * @return
	 */
	static public function deleteScripts( html:String ):String
	{
		var re = ~/<script([\s\S]*?)>([\s\S]*?)<\/script>/g;
		return re.replace( html, "" );
	}

	/**
	 * コメントを削除
	 * @param	html
	 * @return
	 */
	static public function deleteComments( html:String ):String
	{
		var re = ~/<!--([\s\S]*?)-->/g;
		return re.replace( html, "" );
	}

	/**
	 * Path Valueに変換する
	 * @param	html
	 * @return	pathとvalueに変換
	 */
	static public function toPathValue( html:String ):Array<{ path:String, value:String }>
	{
		var results = new Array<{ path:String, value:String }>( );

		var re = ~/<("[^"]*"|'[^']*'|[^'">])*>/g;
		var texts = re.split( html );
		texts.shift( );
		var tags = new Array<String>( );
		re.map( html, function( re ):String {
			var t = re.matched(0);
			tags.push( t );
			return "";
		} );

		var makePath = function( p:Array<String> ) {
			return "/" + p.join( "/" );
		};

		var spaces = ~/\s/g;
		var quotes = ~/".+?"/g;
		var path = new Array<String>( );
		while ( 0 < texts.length ) {
			var tag = tags.shift( );
			switch( tag.substr( 0, 2 ) ) {
				case "<!":
					texts.shift( );
					continue;
				case "</":
					var tagName = spaces.replace( tag.substr( 2 ), "" );
					tagName = tagName.substr( 0, tagName.length - 1 );
					if ( path.exists( function(a) { return a == tagName; } ) ) {
						while ( 0 < path.length ) {
							if ( path.pop( ) == tagName ) break;
						}
					}
				default:
					var hierarchy = true;
					if ( tag.substr( -2 ) == "/>" ) {
						hierarchy = false;
						tag = tag.substr( 1, tag.length - 3 );
					}else {
						tag = tag.substr( 1, tag.length - 2 );
					}
					var list = spaces.split( quotes.replace( tag, "<" ) );
					var quoted = new Array<String>( );
					quotes.map( tag, function( re ) {
						var q = re.matched(0);
						quoted.push( q.substr( 1, q.length - 2 ) );
						return "";
					});
					path.push( list.shift( ) );
					for ( column in list ) {
						var kv = column.split( "=" );
						if ( 2 <= kv.length ) {
							var xpath = makePath( path ) + "@" + kv[0];
							if ( kv[1] == "<" ) {
								results.push( { path:xpath, value:quoted.shift( ) } );
							}else {
								results.push( { path:xpath, value:kv[1] } );
							}
						}
					}
					if ( ! hierarchy ) path.pop( );
			}

			results.push( { path:makePath( path ), value:texts.shift( ) } );
		}

		return results;
	}
}
