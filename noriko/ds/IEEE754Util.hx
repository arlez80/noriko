package noriko.ds;

import haxe.Int64;
import haxe.io.Bytes;

typedef SingleState = {
	sign:Bool,
	exponent:Int,
	fraction:Int,
};

typedef DoubleState = {
	sign:Bool,
	exponent:Int,
	fraction:Int64,
};

/**
 * IEEE 754 Util
 * @author あるる（きのもと 結衣）
 */
class IEEE754Util
{
	/**
	 * 単精度浮動小数点の状態取得
	 * @param	f
	 * @return
	 */
	static public function getSingleState( f:Float ):SingleState
	{
		var b = Bytes.alloc( 4 );
		b.setFloat( 0, f );

		return {
			sign: ( b.getInt32( 0 ) & 0x80000000 ) != 0,
			exponent: ( ( b.getInt32( 0 ) & 0x7f800000 ) >> 23 ) - 127,
			fraction: ( b.getInt32( 0 ) & 0x007fffff ),
		};
	}

	/**
	 * 倍精度浮動小数点の状態取得
	 * @param	f
	 * @return
	 */
	static public function getDoubleState( f:Float ):DoubleState
	{
		var b = Bytes.alloc( 8 );
		b.setDouble( 0, f );

		var frac = Int64.make( ( b.getInt32( 4 ) & 0x000fffff ), b.getInt32( 0 ) );

		return {
			sign: ( b.getInt32( 4 ) & 0x80000000 ) != 0,
			exponent: ( ( b.getInt32( 4 ) & 0x7ff00000 ) >> 20 ) - 1023,
			fraction: frac,
		};
	}

	/**
	 * 正規化する
	 * @param	f
	 * @return
	 */
	static public function normalize( f:Float ):Float
	{
		var b = Bytes.alloc( 8 );
		b.setDouble( 0, f );
		b.setInt32( 4, ( b.getInt32( 4 ) & 0x800fffff ) | 0x3ff00000 );
		return b.getDouble( 0 );
	}

	/**
	 * [-]x.x形式の文字列に変換する
	 * @param	f
	 * @return	[+-]x.x形式の文字列
	 */
	static public function floatToString( f:Float ):String
	{
		if ( Math.isNaN( f ) ) return "nan";
		if ( ! Math.isFinite( f ) ) return "inf";

		// 整数部（手抜き）
		var s = Int64.toStr( Int64.fromFloat( f ) );

		// 小数部（さらに手抜き）
		var nf = 0 < f ? Math.floor( f ) : Math.ceil( f );
		var buf = Int64.toStr( Int64.fromFloat( ( Math.abs( f - nf ) + 1.0 ) * 100000000000 ) );

		// 末尾の0を削除
		var i = buf.length - 1;
		while ( ( buf.charAt( i ) == "0" )&&( 1 < i ) ) i --;
		buf = buf.substr( 1, i );

		if ( 0 < buf.length ) {
			s += "." + buf;
		}

		return s;
	}
}
