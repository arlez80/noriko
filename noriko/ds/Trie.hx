package noriko.ds;

using unifill.Unifill;

/**
 * トライ木
 * @author あるる（きのもと 結衣）
 */
class Trie
{
	private var next:Map<String,Trie>;
	public var value( default, null ):String;
	public var parent( default, null ):Trie;

	/**
	 * コンストラクタ
	 */
	public function new( )
	{
		this.next = new Map<String, Trie>( );
		this.value = null;
		this.parent = null;
	}

	/**
	 * 存在するか？
	 * @param	c
	 * @return
	 */
	public function has( c:String ):Bool
	{
		return this.next.exists( c );
	}

	/**
	 * 取得
	 * @param	c
	 * @return
	 */
	public function get( c:String ):Trie
	{
		return this.next.get( c );
	}

	/**
	 * 文字列追加
	 * @param	s
	 */
	public function add( s:String ):Void
	{
		var leaf = this.addWord( s.uSplit( "" ) );
		leaf.value = s;
	}

	/**
	 * 文字追加
	 * @param	s
	 * @param	i
	 */
	private function addWord( s:Array<String>, i:Int = 0 ):Trie
	{
		if ( s.length <= i ) return this;

		var c = s[i];
		var trie = if ( ! this.next.exists( c ) ) {
			var trie = new Trie( );
			trie.parent = this;
			this.next.set( c, trie );
			trie;
		}else {
			this.next.get( c );
		};
		return trie.addWord( s, i + 1 );
	}

	/**
	 * 文字列検索
	 * @param	s
	 * @return
	 */
	public function find( s:String ):Bool
	{
		return this.findWord( s.uSplit( "" ) );
	}

	/**
	 * 文字列検索
	 * @param	s
	 * @param	i
	 * @return
	 */
	private function findWord( s:Array<String>, i:Int = 0 ):Bool
	{
		if ( s.length <= i ) return true;

		var c = s[i];
		if ( ! this.next.exists( c ) ) return false;
		return this.next.get(c ).findWord( s, i + 1 );
	}
}
