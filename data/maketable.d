/*
	文字コード変換テーブルを作る
		Programed by あるる（きのもと 結衣）

	ftp://ftp.unicode.org/Public/MAPPINGS/OBSOLETE/EASTASIA/JIS/JIS0208.TXT
	上のデータを同じディレクトリに置いて実行する
*/

import std.stdio;
import std.file;
import std.array;
import std.format;

/// JIS->Unicode変換テーブル
ushort[ushort] jis2uni;

void main( )
{
	createJis2UniTable( );

	ushort[65536] toJis, fromJis, toSjis, fromSjis, toEuc, fromEuc;

	for( auto c1 = 0x21; c1 <= 0x7e; c1 ++ ) {
		for( auto c2 = 0x21; c2 <= 0x7e; c2 ++ ) {
			const ushort code = cast(ushort)( ( c1 << 8 ) | ( c2 ) );
			if( code !in jis2uni ) continue;
			const ushort uni = jis2uni[code];

			// JIS
			const ushort jis = cast(ushort)( ( c1 ) | ( c2 << 8 ) );
			toJis[uni] = jis;
			fromJis[jis] = uni;

			// SJIS
			const ushort sjis = jis2sjis( c1, c2 );
			toSjis[uni] = sjis;
			fromSjis[sjis] = uni;

			// EUC
			const ushort euc = jis2euc( c1, c2 );
			toEuc[uni] = euc;
			fromEuc[euc] = uni;
		}
	}

	std.file.write("uni2jis.txt", toJis);
	std.file.write("jis2uni.txt", fromJis);
	std.file.write("uni2sjis.txt", toSjis);
	std.file.write("sjis2uni.txt", fromSjis);
	std.file.write("uni2euc.txt", toEuc);
	std.file.write("euc2uni.txt", fromEuc);
}

void createJis2UniTable( )
{
	auto src = File( "jis0208.txt" );

	foreach( line; src.byLine ) {
		if( line[0] == '#' ) continue;

		auto data = line.split( "\t" );

		int jis, uni;
		formattedRead( data[1], "0x%x", &jis );
		formattedRead( data[2], "0x%x", &uni );

		jis2uni[cast(ushort)jis] = cast(ushort)uni;
	}
}

ushort jis2sjis( const int j1, const int j2 )
{
	int s1, s2;

	if( j1 < 0x5f ) {
		s1 = ((j1 + 1) >> 1 ) + 0x70;
	}else {
		s1 = ((j1 + 1) >> 1 ) + 0xb0;
	}

	if( j1 % 2 == 1 ) {
		if( j2 < 0x60 ) {
			s2 = j2 + 0x1f;
		}else {
			s2 = j2 + 0x20;
		}
	}else {
		s2 = j2 + 0x7e;
	}

	return cast(ushort)( ( s1 ) | ( s2 << 8 ) );
}

ushort jis2euc( const int j1, const int j2 )
{
	return cast(ushort)(
		( j1 | 0x80 ) |
		( ( j2 | 0x80 ) << 8 )
	);
}
