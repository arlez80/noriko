package;

import neko.Lib;
import utest.*;

/**
 * のりこのテスト
 * @author あるる（きのもと 結衣）
 */
class Main 
{
	static function main() 
	{
		var r = new haxe.unit.TestRunner();
		r.add(new TestJCode());
		r.add(new TestJString());
		r.add(new TestCSV());
		r.add(new TestHTML());
		r.add(new TestAhoCorasick());
		r.add(new TestDateUtil());
		r.add(new TestIEEE754Util());
		r.add(new TestQreki());
		r.run();
	}
}
