package utest;
import noriko.str.AhoCorasick;

/**
 * エイホ＝コラシック法テスト
 * @author あるる（きのもと 結衣）
 */
class TestAhoCorasick extends haxe.unit.TestCase
{
	public function checkArray( a:Array<String>, b:Array<String> ):Bool
	{
		if ( a.length != b.length ) return false;

		for ( i in 0 ... a.length ) {
			if ( a[i] != b[i] ) return false;
		}

		return true;
	}

	public function test_getTag( ) {
		var ac = new AhoCorasick();
		ac.words.add("aa");
		ac.words.add("abc");
		assertTrue( this.checkArray( [ "aa", "aa", "abc", "abc" ], ac.match("aaabcdcdcabacabc") ) );

		var ac = new AhoCorasick();
		ac.words.add("ユリィ");
		ac.words.add("アトナ");
		assertTrue( this.checkArray( [ "ユリィ", "アトナ", "アトナ", "ユリィ" ], ac.match("ユリィとアトナは仲良し。アトナの特徴はピンク色の髪で、ユリィはぺたんこ。ユリの花は関係ない、AC法にマッチするか否かのテストアットマーク") ) );
	}
}
