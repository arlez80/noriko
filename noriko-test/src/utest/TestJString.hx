package utest;

import noriko.str.JString;

class TestJString extends haxe.unit.TestCase
{
	public function test_isAlpha() {
		assertTrue( JString.isAlpha("M") );
		assertFalse( JString.isAlpha("ä") );	// 現バージョンではFalse扱い ...
		assertTrue( JString.isAlpha("r") );
		assertTrue( JString.isAlpha("c") );
		assertTrue( JString.isAlpha("h") );
		assertTrue( JString.isAlpha("e") );
		assertTrue( JString.isAlpha("n") );

		assertFalse( JString.isAlpha("き") );
		assertFalse( JString.isAlpha("の") );
		assertFalse( JString.isAlpha("も") );
		assertFalse( JString.isAlpha("と") );
		assertFalse( JString.isAlpha("結") );
		assertFalse( JString.isAlpha("衣") );
	}

	public function test_isNumber() {
		assertTrue( JString.isNumber("0") );
		assertTrue( JString.isNumber("1") );
		assertTrue( JString.isNumber("2") );
		assertTrue( JString.isNumber("3") );
		assertTrue( JString.isNumber("4") );
		assertTrue( JString.isNumber("5") );
		assertTrue( JString.isNumber("6") );
		assertTrue( JString.isNumber("7") );
		assertTrue( JString.isNumber("8") );
		assertTrue( JString.isNumber("9") );

		assertFalse( JString.isNumber("M") );
		assertFalse( JString.isNumber("ä") );
		assertFalse( JString.isNumber("r") );
		assertFalse( JString.isNumber("c") );
		assertFalse( JString.isNumber("h") );
		assertFalse( JString.isNumber("e") );
		assertFalse( JString.isNumber("n") );
		assertFalse( JString.isNumber("き") );
		assertFalse( JString.isNumber("の") );
		assertFalse( JString.isNumber("も") );
		assertFalse( JString.isNumber("と") );
		assertFalse( JString.isNumber("結") );
		assertFalse( JString.isNumber("衣") );
	}

	public function test_isHiragana() {
		assertTrue( JString.isHiragana("き") );
		assertTrue( JString.isHiragana("の") );
		assertTrue( JString.isHiragana("も") );
		assertTrue( JString.isHiragana("と") );
		assertFalse( JString.isHiragana("結") );
		assertFalse( JString.isHiragana("衣") );

		assertFalse( JString.isHiragana("M") );
		assertFalse( JString.isHiragana("ä") );
		assertFalse( JString.isHiragana("r") );
		assertFalse( JString.isHiragana("c") );
		assertFalse( JString.isHiragana("h") );
		assertFalse( JString.isHiragana("e") );
		assertFalse( JString.isHiragana("n") );
	}

	public function test_isKatakana() {
		assertTrue( JString.isKatakana("ホ") );
		assertTrue( JString.isKatakana("ッ") );
		assertTrue( JString.isKatakana("ト") );
		assertTrue( JString.isKatakana("ドッグ") );

		assertFalse( JString.isKatakana("M") );
		assertFalse( JString.isKatakana("ä") );
		assertFalse( JString.isKatakana("r") );
		assertFalse( JString.isKatakana("c") );
		assertFalse( JString.isKatakana("h") );
		assertFalse( JString.isKatakana("e") );
		assertFalse( JString.isKatakana("n") );
	}

	public function test_isHalfKana() {
		assertTrue( JString.isHalfKana("ﾎ") );
		assertTrue( JString.isHalfKana("ｯ") );
		assertTrue( JString.isHalfKana("ﾄ") );
		assertTrue( JString.isHalfKana("ﾄﾞｯｸﾞ") );

		assertFalse( JString.isHalfKana("M") );
		assertFalse( JString.isHalfKana("ä") );
		assertFalse( JString.isHalfKana("r") );
		assertFalse( JString.isHalfKana("c") );
		assertFalse( JString.isHalfKana("h") );
		assertFalse( JString.isHalfKana("e") );
		assertFalse( JString.isHalfKana("n") );
	}

	public function test_isKanji() {
		assertTrue( JString.isKanji("亜") );
		assertTrue( JString.isKanji("井") );
		assertTrue( JString.isKanji("雨") );
		assertTrue( JString.isKanji("絵") );
		assertTrue( JString.isKanji("尾") );

		assertFalse( JString.isKanji("きのもと") );
		assertFalse( JString.isKanji("カタカナ") );
	}

	public function test_levenshtein() {
		assertEquals( 0, JString.levenshtein( "トマト", "トマト" ) );
		assertEquals( 2, JString.levenshtein( "とけい", "となかい" ) );
		assertEquals( 9, JString.levenshtein( "人生宇宙全ての答え", "42" ) );
	}

	public function test_sprintf() {
		assertEquals( "私の身長は 165 cmです", JString.sprintf( "私の身長は %d cmです", [ 165 ] ) );
		assertEquals( "豆乳 100円", JString.sprintf( "豆乳%4d円", [ 100 ] ) );

		// 浮動小数点計算誤差で正常にならない
		assertEquals( "1123.123", JString.sprintf( "%8.3f", [ 51123.123 ], true ) );

		assertEquals( "CPU使用率60% 温度 42.0度", JString.sprintf( "CPU使用率%d%% 温度 %4.1f度", [ 60, 42.0 ] ) );

		assertEquals( "リンゴ 1200円", JString.sprintf( "リンゴ %u円", [ 1200 ] ) );
		assertEquals( "Address 000000173", JString.sprintf( "Address 0%08o", [ 123 ] ) );
		assertEquals( "CALL 012ABH ; CD ab 12", JString.sprintf( "CALL 012ABH ; %2X %2x %2x", [ 0xcd, 0xab, 0x12 ] ) );

		assertEquals( "車体 正面", JString.sprintf( "%1$s %2$s", [ "車体", "正面" ] ) );
		assertEquals( "車体 正面", JString.sprintf( "%2$s %1$s", [ "正面", "車体" ] ) );
	}

	public function test_romajiToKana( ) {
		assertEquals( "ヘックス", JString.romajiToKana( "hekkusu" ) );
		assertEquals( "ミポリン", JString.romajiToKana( "miporin" ) );
	}

	public function test_kanaToRomaji( ) {
		assertEquals( "miporinn", JString.kanaToRomaji( "ミポリン" ) );
		assertEquals( "hekkusu", JString.kanaToRomaji( "ヘックス" ) );
	}

	public function test_findStringPattern( ) {
		var s = "abcdeabcdeobboboboあるる（きのもと 結衣）";
		var answer = [
			{ word:"abcde", count:2 },
			{ word:"o", count:1 },
			{ word:"b", count:2 },
			{ word:"ob", count:2 },
			{ word:"oあ", count:1 },
			{ word:"る", count:2 },
			{ word:"（きのもと 結衣）", count:1 },
		];
		var result = JString.findStringPattern( s );
		assertEquals( answer.length, result.length );
		for ( i in 0 ... answer.length ) {
			assertEquals( answer[i].count, result[i].count );
			assertEquals( answer[i].word, result[i].word );
		}
	}

	public function test_splitAddress( ) {
		var addr = JString.splitAddress( "長崎県佐世保市八幡町1-10" );
		assertEquals( "長崎県", addr.pref );
		assertEquals( "佐世保市", addr.city );
		assertEquals( "八幡町1-10", addr.detail );
		var addr = JString.splitAddress( "長崎県長崎市桜町2-22" );
		assertEquals( "長崎県", addr.pref );
		assertEquals( "長崎市", addr.city );
		assertEquals( "桜町2-22", addr.detail );
		var addr = JString.splitAddress( "長崎県大村市テスト町512-1024" );
		assertEquals( "長崎県", addr.pref );
		assertEquals( "大村市", addr.city );
		assertEquals( "テスト町512-1024", addr.detail );
		var addr = JString.splitAddress( "長崎県のどこか" );
		assertTrue( addr == null );
	}

}