package utest;

import noriko.date.DateUtil;

/**
 * 日付
 * 
 * @author あるる（きのもと 結衣）
 */
class TestDateUtil extends haxe.unit.TestCase
{
	public function makeDate( y:Int, m:Int, d:Int, h:Int = 0, i:Int = 0, s:Int = 0 ):Date
	{
		return new Date( y, m - 1, d, h, i, s );
	}

	public function test_getZodiac10( )
	{
		assertEquals( "丙", DateUtil.getZodiac10( makeDate( 2016, 4, 1 ) ) );
		assertEquals( "庚", DateUtil.getZodiac10( makeDate( 2000, 4, 1 ) ) );
		assertEquals( "庚", DateUtil.getZodiac10( makeDate( 1990, 4, 1 ) ) );
		assertEquals( "庚", DateUtil.getZodiac10( makeDate( 1980, 4, 1 ) ) );
	}

	public function test_getZodiac12( )
	{
		assertEquals( "申", DateUtil.getZodiac12( makeDate( 2016, 4, 1 ) ) );
		assertEquals( "辰", DateUtil.getZodiac12( makeDate( 2000, 4, 1 ) ) );
		assertEquals( "午", DateUtil.getZodiac12( makeDate( 1990, 4, 1 ) ) );
		assertEquals( "申", DateUtil.getZodiac12( makeDate( 1980, 4, 1 ) ) );
	}

	public function test_getBirthstone( )
	{
		assertEquals( "トパーズ", DateUtil.getBirthstone( makeDate( 2016, 11, 28 ) ) );
	}

	public function test_getJapaneseDate( )
	{
		assertEquals( "平成28年11月28日", DateUtil.getJapaneseDate( makeDate( 2016, 11, 28 ) ) );
		assertEquals( "平成元年4月5日", DateUtil.getJapaneseDate( makeDate( 1989, 4, 5 ) ) );
		assertEquals( "昭和64年1月1日", DateUtil.getJapaneseDate( makeDate( 1989, 1, 1 ) ) );
	}

	public function test_getDateInfo( )
	{
		assertEquals( "元旦", DateUtil.getDateInfo( makeDate( 1990, 1, 1 ) ).name );
		// 12月23日
		assertEquals( "", DateUtil.getDateInfo( makeDate( 1990, 12, 23 ) ).name );	// 月曜移動
		assertEquals( "天皇誕生日", DateUtil.getDateInfo( makeDate( 1991, 12, 23 ) ).name );
		// 4月29日
		assertEquals( "天皇誕生日", DateUtil.getDateInfo( makeDate( 1985, 4, 29 ) ).name );
		assertEquals( "", DateUtil.getDateInfo( makeDate( 1990, 4, 29 ) ).name );	// 月曜移動
		assertEquals( "みどりの日", DateUtil.getDateInfo( makeDate( 1991, 4, 29 ) ).name );
		// 5月4日
		assertEquals( "国民の休日", DateUtil.getDateInfo( makeDate( 1988, 5, 4 ) ).name );
		assertEquals( "みどりの日", DateUtil.getDateInfo( makeDate( 2009, 5, 4 ) ).name );
		// 7月の第3月曜
		assertEquals( "海の日", DateUtil.getDateInfo( makeDate( 2017, 7, 17 ) ).name );

		assertEquals( "", DateUtil.getDateInfo( makeDate( 2017, 12, 17 ) ).name );
	}
}
