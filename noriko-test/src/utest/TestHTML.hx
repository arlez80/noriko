package utest;

import noriko.str.HTML;

class TestHTML extends haxe.unit.TestCase
{
	public function test_getElements( ) {
		assertEquals( "<s>noriko</s>", HTML.getElements( "<a><u><s>noriko</s></u></a><s>noriko</s><s>noriko</s><s>noriko</s>", "s" )[0] );
	}

	public function test_getTag( ) {
		assertEquals( "<s>noriko</s>", HTML.getElements( "<a><u><s>noriko</s></u></a>", "s" ).join( "" ) );
		assertEquals( "<u>noriko<s>isobe</s></u>", HTML.getElements( "<a><u>noriko<s>isobe</s></u></a>", "u" ).join( "" ) );
		assertEquals( "<a>noriko</s>", HTML.getElements( "<s><a>noriko</s>", "a" ).join( "" ) );
		assertEquals( "<a href=\"example.com\">test</a>", HTML.getElements( "<z><v><a href=\"example.com\">test</a></v></z>", "a" ).join( "" ) );
		assertEquals( "<a><u><a>noriko</a></u></a>", HTML.getElements( "<a><u><a>noriko</a></u></a>", "a" ).join( "" ) );

		assertEquals( "<a href='index.html'>noriko</a>", HTML.getElements( "<s><a href='index.html'>noriko</a></s>", "a" ).join( "" ) );
	}

	public function test_deleteTag( ) {
		assertEquals( "noriko", HTML.deleteTags( "<a><u><s>noriko</s></u></a>" ) );
		assertEquals( "norikoisobe", HTML.deleteTags( "<a><u>noriko<s>isobe</s></u></a>" ) );
		assertEquals( "noriko", HTML.deleteTags( "<s><a>noriko</s>" ) );
	}

	public function test_getTagTree( ) {
		assertEquals( "<s>noriko</s>", HTML.getElementsByTree( "<a><u><s>noriko</s></u></a>", [ "a", "u", "s" ] ).join( "" ) );
		assertEquals( "<s>isobe</s>", HTML.getElementsByTree( "<a><u>noriko<s>isobe</s></u></a>", [ "u", "s" ] ).join( "" ) );
		assertEquals( "<a>noriko</s>", HTML.getElementsByTree( "<s><a>noriko</s>", [ "s", "a" ] ).join( "" ) );
		assertEquals( "<a href=\"example.com\">test</a>", HTML.getElementsByTree( "<z><v><a href=\"example.com\">test</a></v></z>", [ "v", "a" ] ).join( "" ) );
		assertEquals( "<a>noriko</a>", HTML.getElementsByTree( "<a><u><a>noriko</a></u></a>", [ "a", "u", "a" ] ).join( "" ) );
	}

	public function test_getInnerHTML( ) {
		assertEquals( "noriko", HTML.getInnerHTML( "<s>noriko</s>" ) );
		assertEquals( "<s>noriko</s>", HTML.getInnerHTML( "<u><s>noriko</s></u>" ) );
		assertEquals( "<u><s>noriko</s></u>", HTML.getInnerHTML( "<a><u><s>noriko</s></u></a>" ) );
		assertEquals( "<s class=\"test\">noriko</s>", HTML.getInnerHTML( "<u><s class=\"test\">noriko</s></u>" ) );
	}

	public function test_getAttributes( ) {
		var t = HTML.getAttributes( "<a href='index.html'>てすてす</a>" );
		assertTrue( t.exists("href") );
		assertEquals( "index.html", t.get("href") );

		var t = HTML.getAttributes( "<a href='index.html' target='_blank'><s><test aaa='dfsda' bbb='deldel'>てすてす</a>" );
		assertTrue( t.exists("href") );
		assertEquals( "index.html", t.get("href") );
		assertTrue( t.exists("target") );
		assertEquals( "_blank", t.get("target") );
		assertFalse( t.exists("aaa") );
		assertFalse( t.exists("bbb") );
	}

	public function test_getElementsByAttribute( ) {
		var s = HTML.getElementsByAttribute( "<u><a class=\"happy\">noriko</a><b><c class=\"test\">split<a class=\"ok\">noriko</a>split</c></b></u>", "class" );
		var answers = [
			"<a class=\"happy\">noriko</a>",
			"<c class=\"test\">split<a class=\"ok\">noriko</a>split</c>",
			"<a class=\"ok\">noriko</a>",
		];

		assertEquals( s.length, answers.length );
		var i:Int = 0;
		while ( i < s.length ) {
			assertEquals( answers[i], s[i] );
			i ++;
		}
	}

	public function test_getScripts( ) {
		var s = HTML.getScripts( "<script language=\"test\">hohoho \n<<\n >>></script><script language=\"test\">hohoho2</script>" );
		assertEquals( "hohoho \n<<\n >>>", s[0] );
		assertEquals( "hohoho2", s[1] );
	}

	public function test_deleteScripts( ) {
		var s = HTML.deleteScripts( "isobe<script language=\"test\">hohoho \n<<\n >>></script><script language=\"test\">hohoho2</script> noriko" );
		assertEquals( "isobe noriko", s );
	}

	public function test_getTagName( ) {
		assertEquals( "noriko", HTML.getTagName( "<noriko>" ) );
		assertEquals( "noriko", HTML.getTagName( "<noriko a=\"aaaa\">" ) );
		assertEquals( "noriko", HTML.getTagName( "<noriko>aaaa</noriko>" ) );
		assertEquals( "noriko", HTML.getTagName( "<noriko b=\"bbnbbb\">aaaa</noriko>" ) );
	}

	public function test_toPathValue( ) {
			var html = '<!DOCTYPE rootElement PUBLIC "PublicIdentifier" "URIreference">
	<html>
		<!-- むし --> <!-- 無視 -->
		<a>テストテスト<b>なかてすと</b>テスト
		<c attr="hoge hoge2 hoge3">とじてるテスト</c>
		<!-- ignore -->
		<d bad=123 bad2=あああ good="good attr value">わるい
	</html> ';
		var answer = [
			[ "/html", "" ],
			[ "/html/a", "テストテスト" ],
			[ "/html/a/b", "なかてすと" ],
			[ "/html/a", "テスト" ],
			[ "/html/a/c@attr", "hoge hoge2 hoge3" ],
			[ "/html/a/c", "とじてるテスト" ],
			[ "/html/a", "" ],
			[ "/html/a/d@bad", "123" ],
			[ "/html/a/d@bad2", "あああ" ],
			[ "/html/a/d@good", "good attr value" ],
			[ "/html/a/d", "わるい" ],
			[ "/", "" ],
		];
		var pv = HTML.toPathValue( html );
		assertEquals( answer.length, pv.length );
		var spaces = ~/\s/g;
		for ( i in 0 ... answer.length ) {
			var answerKey = answer[i][0];
			var answerValue = answer[i][1];
			assertEquals( answerKey, pv[i].path );
			assertEquals( answerValue, StringTools.trim( pv[i].value ) );
		}
	}
}
