package utest;

import noriko.date.Qreki;

/**
 * 旧暦テスト
 * @author あるる（きのもと 結衣）
 */
class TestQreki  extends haxe.unit.TestCase
{
	public function test_qreki( )
	{
		var q = Qreki.fromYMD( 2017, 1, 27 );
		assertEquals( 2016, q.year );
		assertEquals( 12, q.month );
		assertEquals( 30, q.date );
		assertEquals( false, q.leapMonth );
		assertEquals( Rokuyou.taian, q.rokuyou );
		var q = Qreki.fromYMD( 2017, 1, 28 );
		assertEquals( 2017, q.year );
		assertEquals( 1, q.month );
		assertEquals( 1, q.date );
		assertEquals( false, q.leapMonth );
		assertEquals( Rokuyou.senshou, q.rokuyou );
		var q = Qreki.fromYMD( 2017, 12, 19 );
		assertEquals( 2017, q.year );
		assertEquals( 11, q.month );
		assertEquals( 2, q.date );
		assertEquals( false, q.leapMonth );
		assertEquals( Rokuyou.shakku, q.rokuyou );
	}
}
