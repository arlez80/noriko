package utest;

import haxe.io.Bytes;
import noriko.str.JCode;

class TestJCode extends haxe.unit.TestCase
{
	public function check( a:String, b:String ):Bool
	{
		var za = Bytes.ofString( a );
		var zb = Bytes.ofString( b );

		if ( za.length != zb.length ) {
			trace( "wrong length! a:" + za.length + " / b:" + zb.length );
			return false;
		}
		if ( za.compare( zb ) == 0 ) return true;

		for ( i in 0 ... za.length ) {
			trace( za.get(i) + ":" + zb.get(i) );
		}
		return false;
	}

	public function test_Conv() {
		var utf8 = CompileTime.readFile( "./data/test_utf8.txt" );
		var jis = CompileTime.readFile( "./data/test_jis.txt" );
		var sjis = CompileTime.readFile( "./data/test_sjis.txt" );
		var euc = CompileTime.readFile( "./data/test_euc.txt" );

		// SJIS
		assertTrue( check( JCode.toSJIS(utf8), sjis ) );
		assertTrue( check( JCode.fromSJIS(sjis), utf8 ) );

		// JIS
		assertTrue( check( JCode.toJIS(utf8), jis ) );
		assertTrue( check( JCode.fromJIS(jis), utf8 ) );

		// EUC
		assertTrue( check( JCode.toEUC(utf8), euc ) );
		assertTrue( check( JCode.fromEUC(euc), utf8 ) );
	}
}
