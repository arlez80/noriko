package utest;

import noriko.ds.IEEE754Util;

/**
 * ...
 * @author あるる（きのもと 結衣）
 */
class TestIEEE754Util extends haxe.unit.TestCase
{
	public function test_Double() {
		assertEquals( "-118.625", IEEE754Util.floatToString( -118.625 ) );
		assertEquals( "0.0078125", IEEE754Util.floatToString( 0.0078125 ) );
		assertEquals( "123.123", IEEE754Util.floatToString( 123.123 ) );
	}
}