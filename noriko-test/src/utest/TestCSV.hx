package utest;

import noriko.str.CSV;

class TestCSV extends haxe.unit.TestCase
{
	public function test_parseCSV() {
		var csv = "a,b,c\n1,2,3";
		var answer = [
			[ "a", "b", "c" ],
			[ "1", "2", "3" ]
		];

		CSV.reader( csv, function( line, data ) {
			var answerLine = answer[line];
			assertEquals( answerLine.length, data.length );
			for( i in 0 ... data.length ) {
				assertEquals( answerLine[i], data[i] );
			}
		} );
	}
}
