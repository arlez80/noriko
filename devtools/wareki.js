var data = [];
var tables = document.getElementsByClassName('wikitable');

for( var i=0; i<tables.length; i++ ) {
	var table = tables[i];

	var trs = table.getElementsByTagName('tr');
	for( var k=0; k<trs.length; k ++ ) {
		var tds = trs[k].getElementsByTagName('td');
		if( tds.length < 2 ) continue;

		var dateStr = tds[1].innerText;
		var s = dateStr.match(/（([0-9]{3,4})年）?\s*([0-9]+)月([0-9]+)日/);
		if( s == null ) continue;
		var year = s[1];
		var month = s[2];
		var date = s[3];
		data.push({
			name: trs[k].getElementsByTagName('th')[0].innerText,
			year: parseInt( year ),
			month: parseInt( month ),
			date: parseInt( date )
		});
	}
}

console.log( JSON.stringify( data.reverse() ) );
