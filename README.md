# Noriko

文字列ライブラリ「のりこ」です。

this is string library "noriko".

## noriko.str.JString

文字列ユーティリティ

utilities for string.

### isAlpha

文字がアルファベットか否かを判定します。

check if charactor is alphabet.

### isUmlautChar

文字がウムラウト有りの文字か否かを判定します。

check if charactor with umlaut.

### isNumber

文字が数字か否かを判定します。

check if charactor is numbers.

### isHiragana

文字がひらがなか否かを判定します。

check if charactor is hiragana.

### isKatakana

文字がカタカナか否かを判定します。

check if charactor is katakana.

### isHalfKana

文字が半角カタカナか否かを判定します。

check if charactor is half-katakana.

### toKatakana

ひらがなをカタカナに変換します。

convert hiragana to katakana.

### toHiragana

カタカナをひらがなに変換します。

convert katakana to hiragana.

### romajiToKana

ローマ字で表記されている文字列をカタカナに変換します。

convert romaji-string to katakana-string.

### kanaToRomaji

カタカナで表記されている文字列をローマ字に変換します。

convert katakana-string to romaji-string.

### splitAddress

日本の住所を [ 県, 市区町村, それ以降 ] に分割します。

split japanese address, ex: [ "pref.", "city", "more..." ]

### levenshtein

文字列のレーベンシュタイン距離を計算します。

levenshtein algorithm.

### strcmp

Cのstrcmpです

this function is strcmp in C.

```
#!haxe

trace( JString.strcmp( "maya", "march" ) );

```

### sprintf

Cのsprintfのように書式変換を行います。

format string like a sprintf in C.

```
#!haxe

trace( JString.sprintf( "score: %06d", [ 12345 ] ) );
trace( JString.sprintf( "name : %s", [ "きのもと 結衣" ] ) );
trace( JString.sprintf( "%s is %d Yen.", [ "orange", 100 ] ) );

```

## noriko.str.AhoCorasick

エイホ＝コラシック法文字列検索

aho-corasick algorithm.

## noriko.str.JCode

文字コードユーティリティ

charactor-code utility.

### toSJIS

文字列をShift-JISに変換します。

convert String to shift-jis

### fromSJIS

文字列をShift-JISから内部形式に変換します。

convert String from shift-jis

### toSJIS

文字列をJISに変換します。

convert String to jis

### fromSJIS

文字列をJISから内部形式に変換します。

convert String from jis

### toEUC

文字列をEUCに変換します。

convert String to euc-jp

### fromEUC

文字列をEUCから内部形式に変換します。

convert String from euc-jp

## noriko.str.Phonetic

### soundex

文字列をsoundexアルゴリズムで変換します。

### caverphone

文字列をcaverphoneアルゴリズムで変換します。

## noriko.str.HTML

HTML解析関連。正規表現を使っているので、きちんとしたXMLでなくてもある程度は読めると思います。

scrap a html. it would be able to read unformatted XML.

### deleteTags

文字列からエレメントを削除します。

delete tag from string.

### toPathValue

pathとvalueの配列を作って返します。属性は@が付いた名前になります。コメントは無視されます。

generate {path, value} array. attributes are added @ to path. ignores comments.

```
#!haxe

trace( HTML.toPathValue( "<html><a href=\"http://haxe.org/\">haxe rules!</a><b>panther<c>ausf.g</c></b></html>" ) );
/*
[
	{ path:"/html/a@href", value:"http://haxe.org/" },
	{ path:"/html/a", value:"haxe rules!" },
	{ path:"/html/a/b", value:"panther" },
	{ path:"/html/a/b/c", value:"ausf.g" },
]
*/
```

### getInnerHTML

エレメントの中身を取得します。

get inner HTML from element.

```
#!haxe

trace( HTML.getInnerHTML( "<a href=\"http://haxe.org/\">haxe rules!</a>" ) );	// haxe rules!

```

### getScripts

文字列からスクリプトを取得します。

get scripts from string.

```
#!haxe

trace( HTML.getScripts( " aabafa<script language=\"test\">hohoho</script> foo <script language=\"test\">hohoho2</script> baar" ) );	// [ "hohoho", "hohoho2" ]


```

### deleteScripts

文字列からスクリプトを削除します

delete scripts from string.

```
#!haxe

trace( HTML.deleteScripts( "isobe<script language=\"test\">hohoho << >>></script><script language=\"test\">hohoho2</script> noriko" ) );	// isobe noriko

```

### deleteComments

文字列からコメントを削除します

delete comments from string.

```
#!haxe

trace( HTML.deleteScripts( "abbbb <!-- babab --> tete" ) );	// abbbb  tete

```

## noriko.str.CSV

コールバック式CSVパーサー

CSV parser using callback method

## noriko.date.DateUtil

日付まわりのユーティリティ

date library for japanese

# データフォルダについて / About Data Folder

## 文字コードテーブル / Charactor Code Table

jcode.zipはこちらのデータより作成いたしました。

"jcode.zip" is generate from this data.

ftp://ftp.unicode.org/Public/MAPPINGS/OBSOLETE/EASTASIA/JIS/JIS0208.TXT

# ライセンス / License

MITライセンス

this library is MIT license.

